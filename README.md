Instruction to deploy empty PopHR
---------------------------------

1. Go to the machine (Ubuntu LTS)

2. Install `` docker-ce``  using  [these instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

3. Install ``docker-compose`` using  [these instructions](https://docs.docker.com/compose/install/#install-compose)

4. 

5. 
```bash
   wget https://git.mchi.mcgill.ca/PHIRE/pophr-deploy/repository/archive.tar.gz?ref=master -O pophr-deploy.tar.gz
   tar -xf pophr-deploy.tar.gz
   mv pophr-deploy-* pophr-deploy
   rm pophr-deploy.tar.gz
   ```
   
6. Extract the folder

7. Download docker image (staging) [jenkins staging](http://132.216.183.86/jenkins/job/pophr/job/staging)
```bash
   curl -o pophr-server-staging-*.tar.gz http://132.216.183.86/jenkins/job/pophr/job/staging/lastSuccessfulBuild/artifact/pophr-server-staging-*.tar.gz -u username:password
   ```

8. Load the docker image in the new environment
```bash
docker load -i pophr-server-staging-*.tar.gz
```

6. Create environment file ``.env`` adjusting values as needed (tag is build number of the docker image)
```bash
POSTGRES_PASSWORD=example
TAG=1
POPHR_PASSWORD=example-pophr-password
ORIGIN=127.0.0.1
```

7. Build the service and bring it online
```bash
docker-compose -f pophr-deploy/docker/docker-compose.yml up -d
```
8. Wait a bit and restart the web container (yeah, I know but its only needed in order to wait for the new db to be initialized)
9. Done

## Enabling Weekly Cleanup

The docker container system will slowly get clogged up since old images won't be automatically removed. It is left to the system administrator to put in place a cleanup process.

Since every PopHR image is ~0.5 GB it accumulates quickly, especially on a system with regular updates. This is why, in tasks, there is a series of Linux Scheduled Tasks to manage this cleanup.

To install do, from the repo root.

```{bash}
cp pophr-deploy/tasks/* /etc/systemd/system
systemctl enable prune.timer
systemctl enable prune.service
systemctl start prune.timer
systemctl start prune.service
```