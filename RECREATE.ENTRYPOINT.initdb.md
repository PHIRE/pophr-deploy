# Info to facilitate recreating the db init files.

## _4.dim.full.sql et _5

* must not have copy statements, use inserts instead
* must both create table and insert content
* must not have any access permissions.


```{sql}
pg_dump -U postgres -h 132.216.183.58 --schema=dim --no-acl --no-owner pophr > _4.dim.full.sql
pg_dump -U postgres -h 132.216.183.58 --schema=obs --no-acl --no-owner pophr > _5.obs.full.sql
```
