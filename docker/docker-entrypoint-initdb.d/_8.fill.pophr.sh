#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname pophr <<-EOSQL

INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (2, 'Incidence Method', '{''count_of_events'',''person_time''}', '{''count_of_events'',''person_time''}', '{''confidence_interval_min'',''confidence_interval_max''}', '''incidence''');
INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (3, 'Identity', '{''value''}', '{''value''}', '{}', '''value''');
INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (1, 'Prevalence Method', '{''count_of_case'',''count_of_people''}', '{''count_of_case'',''count_of_people''}', '{''confidence_interval_min'',''confidence_interval_max''}', '''prevalence''');
INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (6, 'Identity with CI', '{''value'',''ci_lo'',''ci_hi''}', '{''value'',''ci_lo'',''ci_hi''}', '{''confidence_interval_min'',''confidence_interval_max''}', '''value''');
INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (5, 'Arith. Mean Squared Values Precision', '{''value'',''value_squared'',''count''}', '{''value'',''value_squared'',''count''}', '{''std''}', '''mean''');
INSERT INTO pophr.mth (id, name, fields, inputs, details, value) VALUES (4, 'Weighted Arith. Mean No Precision', '{''weight'',''weight_mean'',''weight_mean_mean''}', '{''weight'',''weight_mean'',''weight_mean_mean''}', '{''std''}', '''mean''');

INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs', 'phio:stratification_1yr_age_groups', 'phio:stratification_CT', 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('census_obs', null, 'phio:stratification_CT', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_neigh', null, 'phio:stratification_neighborhood', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_rss_s', null, 'phio:stratification_RSS', 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_clsc', null, 'phio:stratification_CLSC', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_rss', null, 'phio:stratification_RSS', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_global', null, null, null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_rls', null, 'phio:stratification_RLS', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_a_5yr_g_clsc_s', 'phio:stratification_5yr_age_groups', 'phio:stratification_CLSC', 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_s', null, null, 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_a_5yr_s', 'phio:stratification_5yr_age_groups', null, 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_statoid', null, 'phio:ProvinceOrTerritory', null);
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_a_5yr_g_statoid_s', 'phio:stratification_5yr_age_groups', 'phio:ProvinceOrTerritory', 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_g_statoid_s', null, 'phio:ProvinceOrTerritory', 'phio:stratification_male_female');
INSERT INTO pophr.reference_observations (tablename, age, geo, sex) VALUES ('obs_a_5yr_g_statoid', 'phio:stratification_5yr_age_groups', 'phio:ProvinceOrTerritory', null);

EOSQL

