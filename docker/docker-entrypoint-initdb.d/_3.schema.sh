#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname pophr <<-EOSQL

create schema cubes;
  -- Empty Dynamic Cube (holds temporary cache)
create schema data;
  -- Empty Static Cube (holds permanent data)
create schema dim;
  -- Will be loaded later in init
create schema fact;
  -- Empty Fact Tables (holds temporary cache)
create schema obs;
  -- Will be loaded later in init
create schema ref;
  -- Empty Reference Data (used in dyn query and fact tables) (holds permanent data)

create schema pophr;
set search_path to pophr;

create table causal_concept_to_cubes (
  id         serial primary key,
  indicator  varchar(250),
  cube       varchar(250),
  obs        varchar(250),
  script     text,
  aggregator varchar(50),
  fk_fact    integer,
  use_data   boolean,
  mth_fk     integer,
  fk_facts   integer []
);

comment on table causal_concept_to_cubes is 'Indicator data cubes with reference observations';
comment on column causal_concept_to_cubes.id is 'Internal ID';
comment on column causal_concept_to_cubes.indicator is 'Indicator this data is for';
comment on column causal_concept_to_cubes.cube is 'Tablename for the cube containing the data';
comment on column causal_concept_to_cubes.obs is 'Population this data references';
comment on column causal_concept_to_cubes.script is 'Script to regenerate the cube if dynamic';
comment on column causal_concept_to_cubes.aggregator is 'Keyword to aggregate in SQL Select (ex. SUM)';
comment on column causal_concept_to_cubes.fk_fact is 'obselete';
comment on column causal_concept_to_cubes.use_data is 'Is using static data?';
comment on column causal_concept_to_cubes.mth_fk is 'Foreign Key to a Method.';
comment on column causal_concept_to_cubes.fk_facts is 'Multiple Foreign Key for Each Fact Dependencies.';

create table fact (
  id        serial primary key,
  script    text,
  tablename varchar(250),
  conf      json,
  name      text
);

comment on table fact is 'Stores Fact Tables';
comment on column fact.id is 'Internal ID';
comment on column fact.script is 'Script to regenerate the fact table';
comment on column fact.tablename is 'Tablename of the Fact Table';
comment on column fact.conf is 'obselete';
comment on column fact.name is 'Human Readable Short Name for the Fact Table.';

create table methodology (
  shortform varchar(250) not null,
  edited    timestamp    not null,
  file      bytea        not null,
  hash      char(32),
  locale    char(2),
  primary key (shortform, edited)
);

comment on table methodology is 'Stores the methodology that are associated with certain indicators.';
comment on column methodology.shortform is 'This methodology''s indicator';
comment on column methodology.edited is 'Last time it was edited';
comment on column methodology.file is 'PDF byte[]';
comment on column methodology.hash is 'Hash of the PDF for comparison';
comment on column methodology.locale is 'Locale of this document';
create index idx_methodology_shortform on methodology (shortform);

create table mth (
  id      serial,
  name    varchar(250) not null,
  fields  varchar(250) [],
  inputs  varchar(250) [],
  details varchar(250) [],
  value   varchar(250),
  primary key (id)
);

comment on table mth is 'List of available methods';
comment on column mth.id is 'Internal ID';
comment on column mth.name is 'Human Readable Description';
comment on column mth.fields is 'deprecated';
comment on column mth.inputs is 'Required fields';
comment on column mth.details is 'Fields name of additional details included in the output';
comment on column mth.value is 'Name of the value field';

create table reference_observations (
  tablename varchar(50),
  age       varchar(100),
  geo       varchar(100),
  sex       varchar(100),
  primary key(tablename)
);

comment on table reference_observations is 'Reference observations used by the cubes';
comment on column reference_observations.tablename is 'Tablename in the obs schema';
comment on column reference_observations.age is 'Shortform for the age dimension in this obs';
comment on column reference_observations.geo is 'Shortform for the geo dimension in this obs';
comment on column reference_observations.sex is 'Shortform for the sex dimension in this obs';

create function extract_interval(range tsrange) returns interval
  as 'select CASE WHEN isempty(range) THEN INTERVAL ''0'' ELSE upper(range) - lower(range) END;'
  language SQL
  IMMUTABLE
  RETURNS NULL ON NULL INPUT;

-- Standardizations tables will be loaded later.

create schema usage;
set search_path to usage;

create table "user" (
  roleid integer default 1,
  name char(300),
  email char(300),
  uid char(300) primary key,
  createdon timestamp with time zone default now(),
  lastseen timestamp with time zone default now(),
  lastmodificationtime timestamp with time zone default now(),
  experiment integer,
  groupname char(300) default ''::bpchar,
  studypopulation jsonb
);

comment on table "user" is 'Table of our users.';
comment on column "user".roleid is 'Role ID as defined in the enumeration org.mcgill.phire.pophr.server.user.Role.';
comment on column "user".uid is 'Unique ID identifying the user';

create table user_dashboard (
  id serial primary key,
  name varchar(100) not null,
  note varchar(300),
  uid varchar(300) not null references "user",
  create_on timestamp default now() not null
);

create table user_dashboardspot (
  id serial primary key,
  img text not null,
  link varchar(300) not null,
  note varchar(300),
  dashboard_id integer not null references user_dashboard,
  created_on timestamp with time zone default now()
);

create table user_feedback (
  id serial primary key,
  feedback varchar(1000) not null,
  ip varchar,
  browser varchar(50) default NULL::character varying,
  url text default NULL::character varying,
  ts timestamp default now(),
  uid varchar(30)
);

create table query_metrics (
  qid bigserial primary key,
  duration double precision not null,
  timestart timestamp not null,
  timeend timestamp not null,
  endpoint varchar(300),
  type varchar(300),
  iscached boolean not null,
  params varchar(5000),
  path varchar(300)
);

create table metrics (
  qid bigserial primary key,
  duration double precision not null,
  timestart timestamp not null,
  timeend timestamp not null,
  endpoint varchar(300),
  type varchar(300),
  params varchar(5000),
  iscached boolean not null
);

create table themes (
  tid bigint primary key,
  created_by varchar(30) [],
  time_created timestamp not null,
  content varchar(1000),
  isactive boolean default false,
  summary varchar(30),
  env varchar(30)
);

set search_path to ref;
create materialized view years as
SELECT
  (dat.year)::date AS year,
  daterange((dat.year)::date, ((dat.year + '1 year' :: interval))::date) AS range
FROM (
  SELECT generate_series('1998-01-01'::timestamp, now(), '1 year'::interval) AS year
) dat;

comment on materialized view years is 'Years since 1998';
comment on column years.year is 'Date the year began';
comment on column years.range is 'Range of dates in this years';


-- Going back to the default search path

set search_path to public;

EOSQL