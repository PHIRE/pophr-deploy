\c pophr

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: a_10yr; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_10yr (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: a_1to10yr; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_1to10yr (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: a_1to5yr; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_1to5yr (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: a_1tocae; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_1tocae (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: a_5yr; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_5yr (
    id integer NOT NULL,
    shortform character varying(250),
    range int4range
);


--
-- Name: a_cae; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.a_cae (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: codelists; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.codelists (
    shortform character varying(250) NOT NULL,
    tablename character varying(250) NOT NULL
);


--
-- Name: g_clsc; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_clsc (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: g_clsc2rls; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_clsc2rls (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_clsc2rss; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_clsc2rss (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_clsc2statoid; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_clsc2statoid (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_ct; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct (
    id integer NOT NULL,
    official character(10) NOT NULL,
    in2001 boolean,
    in2006 boolean,
    in2011 boolean,
    cma integer
);


--
-- Name: TABLE g_ct; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON TABLE dim.g_ct IS 'Information about Montreals Census Tract. (Important to know CT are not available everywhere in Quebec)';


--
-- Name: COLUMN g_ct.id; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_ct.id IS 'Internal ID';


--
-- Name: COLUMN g_ct.official; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_ct.official IS 'Reference ID used by external entities.';


--
-- Name: g_ct2clsc; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct2clsc (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: TABLE g_ct2clsc; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON TABLE dim.g_ct2clsc IS 'Mapping tables for the conversion between CT and CLSC using internal IDs.
-- Adjusted on 2016-06-30 by Maxime using a conversion table provided by Guido.';


--
-- Name: g_ct2neigh; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct2neigh (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_ct2rls; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct2rls (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_ct2rss; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct2rss (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_ct2statoid; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_ct2statoid (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_neigh; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_neigh (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: g_rls; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_rls (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: g_rls2rss; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_rls2rss (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_rls2statoid; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_rls2statoid (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_rss; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_rss (
    id integer NOT NULL,
    shortform character varying(250)
);


--
-- Name: g_rss2statoid; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_rss2statoid (
    src integer NOT NULL,
    dst integer NOT NULL
);


--
-- Name: g_statoid; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.g_statoid (
    id integer NOT NULL,
    shortform character varying(250),
    abbrev character(2) NOT NULL,
    en character(25) NOT NULL,
    fr character(25) NOT NULL
);


--
-- Name: TABLE g_statoid; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON TABLE dim.g_statoid IS 'Provinces/Territories';


--
-- Name: COLUMN g_statoid.id; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_statoid.id IS 'Internal ID';


--
-- Name: COLUMN g_statoid.shortform; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_statoid.shortform IS 'KB Shortform Reference';


--
-- Name: COLUMN g_statoid.abbrev; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_statoid.abbrev IS 'Official Abbreviations';


--
-- Name: COLUMN g_statoid.en; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_statoid.en IS 'English Name';


--
-- Name: COLUMN g_statoid.fr; Type: COMMENT; Schema: dim; Owner: -
--

COMMENT ON COLUMN dim.g_statoid.fr IS 'French Name';


--
-- Name: mappings; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.mappings (
    type character varying(50),
    src character varying(250) NOT NULL,
    dst character varying(250) NOT NULL,
    version integer NOT NULL,
    tablename character varying(250)
);


--
-- Name: sex; Type: TABLE; Schema: dim; Owner: -
--

CREATE TABLE dim.sex (
    id integer NOT NULL,
    shortform character varying(250) NOT NULL
);


--
-- Data for Name: a_10yr; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_10yr (id, shortform) FROM stdin;
1	phio:ageCategory_0-9_year_old
2	phio:ageCategory_10-19_year_old
3	phio:ageCategory_20-29_year_old
4	phio:ageCategory_30-39_year_old
5	phio:ageCategory_40-49_year_old
6	phio:ageCategory_50-59_year_old
7	phio:ageCategory_60-69_year_old
8	phio:ageCategory_70-79_year_old
9	phio:ageCategory_80-89_year_old
10	phio:ageCategory_90-99_year_old
11	phio:ageCategory_over_99
\.


--
-- Data for Name: a_1to10yr; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_1to10yr (src, dst) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	2
11	2
12	2
13	2
14	2
15	2
16	2
17	2
18	2
19	2
20	3
21	3
22	3
23	3
24	3
25	3
26	3
27	3
28	3
29	3
30	4
31	4
32	4
33	4
34	4
35	4
36	4
37	4
38	4
39	4
40	5
41	5
42	5
43	5
44	5
45	5
46	5
47	5
48	5
49	5
50	6
51	6
52	6
53	6
54	6
55	6
56	6
57	6
58	6
59	6
60	7
61	7
62	7
63	7
64	7
65	7
66	7
67	7
68	7
69	7
70	8
71	8
72	8
73	8
74	8
75	8
76	8
77	8
78	8
79	8
80	9
81	9
82	9
83	9
84	9
85	9
86	9
87	9
88	9
89	9
90	10
91	10
92	10
93	10
94	10
95	10
96	10
97	10
98	10
99	10
100	11
101	11
102	11
103	11
104	11
105	11
106	11
107	11
108	11
109	11
110	11
111	11
112	11
113	11
114	11
115	11
116	11
117	11
118	11
119	11
120	11
121	11
122	11
123	11
124	11
125	11
126	11
127	11
128	11
129	11
130	11
131	11
132	11
133	11
134	11
135	11
136	11
137	11
138	11
139	11
140	11
141	11
142	11
143	11
144	11
145	11
146	11
147	11
148	11
149	11
\.


--
-- Data for Name: a_1to5yr; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_1to5yr (src, dst) FROM stdin;
1	0
2	0
3	0
4	0
5	1
6	1
7	1
8	1
9	1
10	2
11	2
12	2
13	2
14	2
15	3
16	3
17	3
18	3
19	3
20	4
21	4
22	4
23	4
24	4
25	5
26	5
27	5
28	5
29	5
30	6
31	6
32	6
33	6
34	6
35	7
36	7
37	7
38	7
39	7
40	8
41	8
42	8
43	8
44	8
45	9
46	9
47	9
48	9
49	9
50	10
51	10
52	10
53	10
54	10
55	11
56	11
57	11
58	11
59	11
60	12
61	12
62	12
63	12
64	12
65	13
66	13
67	13
68	13
69	13
70	14
71	14
72	14
73	14
74	14
75	15
76	15
77	15
78	15
79	15
80	16
81	16
82	16
83	16
84	16
85	17
86	17
87	17
88	17
89	17
90	18
91	18
92	18
93	18
94	18
95	19
96	19
97	19
98	19
99	19
100	20
101	20
102	20
103	20
104	20
105	20
106	20
107	20
108	20
109	20
110	20
111	20
112	20
113	20
114	20
115	20
116	20
117	20
118	20
119	20
120	20
121	20
122	20
123	20
124	20
125	20
126	20
127	20
128	20
129	20
130	20
131	20
132	20
133	20
134	20
135	20
136	20
137	20
138	20
139	20
140	20
141	20
142	20
143	20
144	20
145	20
146	20
147	20
148	20
149	20
\.


--
-- Data for Name: a_1tocae; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_1tocae (src, dst) FROM stdin;
65	3
66	3
67	3
68	3
69	3
70	3
71	3
72	3
73	3
74	3
75	3
76	3
77	3
78	3
79	3
80	3
81	3
82	3
83	3
84	3
85	3
86	3
87	3
88	3
89	3
90	3
91	3
92	3
93	3
94	3
95	3
96	3
97	3
98	3
99	3
100	3
101	3
102	3
103	3
104	3
105	3
106	3
107	3
108	3
109	3
110	3
111	3
112	3
113	3
114	3
115	3
116	3
117	3
118	3
119	3
120	3
121	3
122	3
123	3
124	3
125	3
126	3
127	3
128	3
129	3
130	3
131	3
132	3
133	3
134	3
135	3
136	3
137	3
138	3
139	3
140	3
141	3
142	3
143	3
144	3
145	3
146	3
147	3
148	3
149	3
18	2
19	2
20	2
21	2
22	2
23	2
24	2
25	2
26	2
27	2
28	2
29	2
30	2
31	2
32	2
33	2
34	2
35	2
36	2
37	2
38	2
39	2
40	2
41	2
42	2
43	2
44	2
45	2
46	2
47	2
48	2
49	2
50	2
51	2
52	2
53	2
54	2
55	2
56	2
57	2
58	2
59	2
60	2
61	2
62	2
63	2
64	2
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
\.


--
-- Data for Name: a_5yr; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_5yr (id, shortform, range) FROM stdin;
1	phio:ageCategory_5-9_year_old	[5,10)
0	phio:ageCategory_0-4_year_old	[0,5)
7	phio:ageCategory_35-39_year_old	[35,40)
4	phio:ageCategory_20-24_year_old	[20,25)
16	phio:ageCategory_80-84_year_old	[80,85)
11	phio:ageCategory_55-59_year_old	[55,60)
17	phio:ageCategory_85-89_year_old	[85,90)
10	phio:ageCategory_50-54_year_old	[50,55)
19	phio:ageCategory_95-99_year_old	[95,100)
14	phio:ageCategory_70-74_year_old	[70,75)
9	phio:ageCategory_45-49_year_old	[45,50)
2	phio:ageCategory_10-14_year_old	[10,15)
3	phio:ageCategory_15-19_year_old	[15,20)
12	phio:ageCategory_60-64_year_old	[60,65)
5	phio:ageCategory_25-29_year_old	[25,30)
6	phio:ageCategory_30-34_year_old	[30,35)
20	phio:ageCategory_over_99	[100,)
15	phio:ageCategory_75-79_year_old	[75,80)
13	phio:ageCategory_65-69_year_old	[65,70)
8	phio:ageCategory_40-44_year_old	[40,45)
18	phio:ageCategory_90-94_year_old	[90,95)
\.


--
-- Data for Name: a_cae; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.a_cae (id, shortform) FROM stdin;
1	phio:ageCategory_children_under_18
2	phio:ageCategory_18-65
3	phio:ageCategory_over_64
\.


--
-- Data for Name: codelists; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.codelists (shortform, tablename) FROM stdin;
phio:stratification_RSS	g_rss
phio:stratification_neighborhood	g_neigh
phio:stratification_CLSC	g_clsc
phio:stratification_5yr_age_groups	a_5yr
phio:stratification_10yr_age_groups	a_10yr
phio:stratification_children_adults_elderly	a_cae
phio:stratification_male_female	sex
phio:stratification_RLS	g_rls
phio:ProvinceOrTerritory	g_statoid
\.


--
-- Data for Name: g_clsc; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_clsc (id, shortform) FROM stdin;
6062	geo:clsc_Saint-Michel
9031	geo:clsc_Port-Cartier
3026	geo:clsc_Duberger-Les_Saules-Lebourgneuf
1011	geo:clsc_Rimouski-Neigette
11051	geo:clsc_Denis-Riverin
6044	geo:clsc_Anjou
6053	geo:clsc_Rosemont
3041	geo:clsc_Charlevoix-Est
16061	geo:clsc_Longueuil-Ouest
1071	geo:clsc_Riviere-du-Loup
8041	geo:clsc_Abitibi-Ouest
6091	geo:clsc_Saint-Louis-du-Parc
9061	geo:clsc_Minganie
17101	geo:clsc_Baie_D_Hudson
18101	geo:clsc_Territoire_Cri
14021	geo:clsc_Les_Moulins
8071	geo:clsc_Temiscaming
12025	geo:clsc_La_Nouvelle-Beauce
3031	geo:clsc_Loretteville_-_Val-Belair
4051	geo:clsc_Trois-Rivieres
10104	geo:clsc_Baie-James
16051	geo:clsc_Saint-Hubert
16111	geo:clsc_Granby-Shefford-Bromont
6121	geo:clsc_Villeray
4041	geo:clsc_Maskinonge
4021	geo:clsc_Mekinac
3021	geo:clsc_Laurentien
6051	geo:clsc_Mercier-Ouest
14011	geo:clsc_D_Autray
11012	geo:clsc_Avignon
16052	geo:clsc_Brossard_-_Saint-Lambert
12031	geo:clsc_Beauce-Sartigan
16062	geo:clsc_Longueuil-Est
1061	geo:clsc_Temiscouata
12032	geo:clsc_Robert-Cliche
7013	geo:clsc_Gatineau
16043	geo:clsc_Saint-Constant_-_La_Prairie
15051	geo:clsc_Les_Pays-d_en-Haut
16042	geo:clsc_Les_Jardins-de-Napierville
16082	geo:clsc_Les_Maskoutains
5051	geo:clsc_Coaticook
6073	geo:clsc_Mont-Royal
12052	geo:clsc_Montmagny
4061	geo:clsc_Nicolet-Yamaska
1031	geo:clsc_Matane
6122	geo:clsc_Petite_Patrie
4052	geo:clsc_Cap-de-la-Madeleine
8051	geo:clsc_Abitibi
8072	geo:clsc_Ville-Marie
6061	geo:clsc_Saint-Leonard
5071	geo:clsc_Sherbrooke
7052	geo:clsc_Petite-Nation
12011	geo:clsc_Des_Etchemins
6072	geo:clsc_Snowdon
10103	geo:clsc_Matagami
6081	geo:clsc_Cote-Saint-Luc
8031	geo:clsc_Rouyn-Noranda
1081	geo:clsc_Kamouraska
6042	geo:clsc_Pointe-aux-Trembles
13011	geo:clsc_Duvernay
7011	geo:clsc_Hull
14012	geo:clsc_Matawinie
4081	geo:clsc_Arthabaska
2021	geo:clsc_Saguenay
6074	geo:clsc_Metro
14022	geo:clsc_L_Assomption
2041	geo:clsc_Domaine-du-Roy
3011	geo:clsc_Portneuf
3023	geo:clsc_Quebec_-_Haute-Ville
11021	geo:clsc_Pabok
6092	geo:clsc_Montreal_-_Centre-Sud
12024	geo:clsc_Lotbiniere
3034	geo:clsc_Charlesbourg
6012	geo:clsc_Pierrefonds
9081	geo:clsc_Territoire_Naskapi
9041	geo:clsc_Sept-Iles
3024	geo:clsc_Quebec_-_Basse-Ville
10101	geo:clsc_Chibougamau-Chapais
5021	geo:clsc_Asbestos
16031	geo:clsc_Valleyfield-Beauharnois
4062	geo:clsc_Becancour
9051	geo:clsc_Caniapiscau
11031	geo:clsc_Gaspe
12023	geo:clsc_Bellechasse
6041	geo:clsc_Riviere-des-Prairies
5041	geo:clsc_Val_Saint-Francois
11041	geo:clsc_Iles-de-la-Madeleine
6075	geo:clsc_Parc-Extension
6112	geo:clsc_Saint-Laurent
1041	geo:clsc_La_Matapedia
6034	geo:clsc_Saint-Henri
16101	geo:clsc_Cowansville-Farnham-Bedford
7012	geo:clsc_Aylmer
13014	geo:clsc_Sainte-Rose-de-Laval
16121	geo:clsc_Vaudreuil-Soulanges
14014	geo:clsc_Montcalm
6011	geo:clsc_Lac_Saint-Louis
16091	geo:clsc_Bas_Richelieu
16041	geo:clsc_Chateauguay-Mercier
8061	geo:clsc_Vallee-de-l_Or
6013	geo:clsc_Dollard-des-Ormeaux
5061	geo:clsc_Memphremagog
6093	geo:clsc_Plateau-Mont-Royal
17102	geo:clsc_Ungava
12021	geo:clsc_Desjardins
15031	geo:clsc_Therese-De_Blainville
9012	geo:clsc_Forestville
16083	geo:clsc_Acton
4022	geo:clsc_Des_Chenaux
9011	geo:clsc_Les_Escoumins
2051	geo:clsc_Maria-Chapdelaine
3042	geo:clsc_Charlevoix-Ouest
13012	geo:clsc_Chomedey
15061	geo:clsc_Les_Laurentides
15021	geo:clsc_La_Riviere-du-Nord_-_Mirabel
3022	geo:clsc_Sainte-Foy_-_Sillery
6132	geo:clsc_Ahuntsic
3033	geo:clsc_Orleans
5011	geo:clsc_Granit
6131	geo:clsc_Montreal-Nord
1021	geo:clsc_La_Mitis
16081	geo:clsc_Saint-Bruno_-_Beloeil_-_Saint-Hilaire
16072	geo:clsc_Chambly-Carignan-Marieville
4071	geo:clsc_Drummond
6032	geo:clsc_Verdun
7021	geo:clsc_Pontiac
9071	geo:clsc_Basse_Cote-Nord
12041	geo:clsc_Les_Appalaches
6111	geo:clsc_Bordeaux-Cartierville
2061	geo:clsc_Lac-Saint-Jean-Est
13013	geo:clsc_Pont-Viau
6031	geo:clsc_Pointe-Saint-Charles
9021	geo:clsc_Manicouagan
12051	geo:clsc_L_Islet
6071	geo:clsc_Cote-des-Neiges
16021	geo:clsc_Haut-Saint-Laurent
11033	geo:clsc_Murdochville
7041	geo:clsc_Des_Forestiers
12022	geo:clsc_Les_Chutes-de-la-Chaudiere
6082	geo:clsc_Notre-Dame-de-Graces_-_Montreal-Ouest
15041	geo:clsc_Antoine-Labelle
14013	geo:clsc_Joliette
7031	geo:clsc_Les_Collines-de-l_Outaouais
5031	geo:clsc_Haut-Saint-Francois
3025	geo:clsc_Limoilou-Vanier
6043	geo:clsc_Mercier-Est
6094	geo:clsc_Montreal_-_Centre-Ville
7051	geo:clsc_Vallee-de-la-Lievre
6033	geo:clsc_Saint-Paul
11011	geo:clsc_Bonaventure
4082	geo:clsc_De_l_Erable
3032	geo:clsc_Beauport
15071	geo:clsc_Argenteuil
10102	geo:clsc_Lebel-sur-Quevillon
1051	geo:clsc_Les_Basques
6021	geo:clsc_Lachine
4031	geo:clsc_Centre-de-la-Mauricie
15011	geo:clsc_Deux-Montagnes_-_Mirabel
16071	geo:clsc_Saint-Jean-sur-Richelieu_-_Saint-Luc
6022	geo:clsc_Lasalle
6052	geo:clsc_Hochelaga-Maisonneuve
11032	geo:clsc_Grande-Vallee
4011	geo:clsc_Haut-Saint-Maurice
2011	geo:clsc_Fjord
16063	geo:clsc_Lajemmerais
2031	geo:clsc_Jonquiere
\.


--
-- Data for Name: g_clsc2rls; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_clsc2rls (src, dst) FROM stdin;
3021	302
1011	101
1021	102
1031	103
1041	104
1051	105
1061	106
1071	107
1081	108
2011	201
2021	202
2031	203
2041	204
2051	205
2061	206
3011	301
3022	302
3023	302
3024	302
3025	302
3026	302
3031	303
3032	303
3033	303
3034	303
3041	304
3042	304
4011	401
4021	402
4022	402
4031	403
4041	404
4051	405
4052	405
4061	406
4062	406
4071	407
4081	408
4082	408
5011	501
5021	502
5031	503
5041	504
5051	505
5061	506
5071	507
6011	601
6012	601
6013	601
6021	602
6022	602
6031	603
6032	603
6033	603
6034	603
6041	604
6042	604
6043	604
6044	604
6051	605
6052	605
6053	605
6061	606
6062	606
6071	607
6072	607
6073	607
6074	607
6075	607
6081	608
6082	608
6091	609
6092	609
6093	609
6094	609
6111	611
6112	611
6121	612
6122	612
6131	613
6132	613
7011	701
7012	701
7013	701
7021	702
7031	703
7041	704
7051	705
7052	705
8031	803
8041	804
8051	805
8061	806
8071	807
8072	807
9011	901
9012	901
9021	902
9031	903
9041	904
9051	905
9061	906
9071	907
9081	908
11011	1101
11012	1101
11021	1102
11031	1103
11032	1103
11033	1103
11041	1104
11051	1105
12011	1201
12021	1202
12022	1202
12023	1202
12024	1202
12025	1202
12031	1203
12032	1203
12041	1204
12051	1205
12052	1205
13011	1301
13012	1301
13013	1301
13014	1301
14011	1401
14012	1401
14013	1401
14014	1401
14021	1402
14022	1402
15011	1501
15021	1502
15031	1503
15041	1504
15051	1505
15061	1506
15071	1507
16021	1602
16031	1603
16041	1604
16042	1604
16043	1604
16051	1605
16052	1605
16061	1606
16062	1606
16063	1606
16071	1607
16072	1607
16081	1608
16082	1608
16083	1608
16091	1609
16101	1610
16111	1611
16121	1612
10101	1010
10102	1010
10103	1010
10104	1010
17101	1710
17102	1710
18101	1810
\.


--
-- Data for Name: g_clsc2rss; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_clsc2rss (src, dst) FROM stdin;
1011	1
1021	1
1031	1
1041	1
1051	1
1061	1
1071	1
1081	1
2011	2
2021	2
2031	2
2041	2
2051	2
2061	2
3011	3
3021	3
3022	3
3023	3
3024	3
3025	3
3026	3
3031	3
3032	3
3033	3
3034	3
3041	3
3042	3
4011	4
4021	4
4022	4
4031	4
4041	4
4051	4
4052	4
4061	4
4062	4
4071	4
4081	4
4082	4
5011	5
5021	5
5031	5
5041	5
5051	5
5061	5
5071	5
6011	6
6012	6
6013	6
6021	6
6022	6
6031	6
6032	6
6033	6
6034	6
6041	6
6042	6
6043	6
6044	6
6051	6
6052	6
6053	6
6061	6
6062	6
6071	6
6072	6
6073	6
6074	6
6075	6
6081	6
6091	6
6092	6
6093	6
6094	6
6111	6
6112	6
6121	6
6122	6
6131	6
6132	6
7011	7
7012	7
7013	7
7021	7
7031	7
7041	7
7051	7
7052	7
8031	8
8041	8
8051	8
8061	8
8071	8
8072	8
9011	9
9012	9
9021	9
9031	9
9041	9
9051	9
9061	9
9071	9
9081	9
11011	11
11012	11
11021	11
11031	11
11032	11
11033	11
11041	11
11051	11
12011	12
12021	12
12022	12
12023	12
12024	12
12025	12
12032	12
12031	12
12041	12
12051	12
12052	12
13011	13
13012	13
13013	13
13014	13
14011	14
14012	14
14013	14
14014	14
15011	15
15021	15
15031	15
15041	15
15051	15
15061	15
15071	15
16021	16
16031	16
16041	16
16042	16
16043	16
16051	16
16052	16
16061	16
16062	16
16063	16
16071	16
16072	16
16081	16
16082	16
16083	16
16091	16
16101	16
16111	16
16121	16
6082	6
14021	14
14022	14
10101	10
10102	10
10103	10
10104	10
17101	17
17102	17
18101	18
\.


--
-- Data for Name: g_clsc2statoid; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_clsc2statoid (src, dst) FROM stdin;
6062	10
9031	10
3026	10
1011	10
11051	10
6044	10
6053	10
3041	10
16061	10
1071	10
8041	10
6091	10
9061	10
17101	10
18101	10
14021	10
8071	10
12025	10
3031	10
4051	10
10104	10
16051	10
16111	10
6121	10
4041	10
4021	10
3021	10
6051	10
14011	10
11012	10
16052	10
12031	10
16062	10
1061	10
12032	10
7013	10
16043	10
15051	10
16042	10
16082	10
5051	10
6073	10
12052	10
4061	10
1031	10
6122	10
4052	10
8051	10
8072	10
6061	10
5071	10
7052	10
12011	10
6072	10
10103	10
6081	10
8031	10
1081	10
6042	10
13011	10
7011	10
14012	10
4081	10
2021	10
6074	10
14022	10
2041	10
3011	10
3023	10
11021	10
6092	10
12024	10
3034	10
6012	10
9081	10
9041	10
3024	10
10101	10
5021	10
16031	10
4062	10
9051	10
11031	10
12023	10
6041	10
5041	10
11041	10
6075	10
6112	10
1041	10
6034	10
16101	10
7012	10
13014	10
16121	10
14014	10
6011	10
16091	10
16041	10
8061	10
6013	10
5061	10
6093	10
17102	10
12021	10
15031	10
9012	10
16083	10
4022	10
9011	10
2051	10
3042	10
13012	10
15061	10
15021	10
3022	10
6132	10
3033	10
5011	10
6131	10
1021	10
16081	10
16072	10
4071	10
6032	10
7021	10
9071	10
12041	10
6111	10
2061	10
13013	10
6031	10
9021	10
12051	10
6071	10
16021	10
11033	10
7041	10
12022	10
6082	10
15041	10
14013	10
7031	10
5031	10
3025	10
6043	10
6094	10
7051	10
6033	10
11011	10
4082	10
3032	10
15071	10
10102	10
1051	10
6021	10
4031	10
15011	10
16071	10
6022	10
6052	10
11032	10
4011	10
2011	10
16063	10
2031	10
\.


--
-- Data for Name: g_ct; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct (id, official, in2001, in2006, in2011, cma) FROM stdin;
526	0627.00   	t	t	t	462
527	0628.01   	t	t	t	462
528	0628.02   	t	t	t	462
529	0629.00   	t	t	t	462
530	0630.01   	t	t	t	462
531	0630.02   	t	t	t	462
532	0631.00   	t	t	t	462
533	0632.01   	t	t	t	462
552	0643.00   	t	t	f	462
578	0656.02   	t	t	f	462
585	0659.02   	t	t	f	462
589	0659.07   	t	t	f	462
594	0661.02   	t	t	f	462
603	0677.03   	t	t	f	462
616	0684.04   	t	t	f	462
618	0684.06   	t	t	f	462
628	0687.02   	t	t	f	462
648	0701.00   	t	t	f	462
668	0710.01   	t	t	f	462
669	0710.02   	t	t	f	462
671	0710.04   	t	t	f	462
693	0734.02   	t	t	f	462
696	0740.00   	t	t	f	462
699	0750.03   	t	t	f	462
709	0758.01   	t	t	f	462
710	0758.02   	t	t	f	462
714	0777.00   	t	t	f	462
732	0804.00   	t	t	f	462
751	0826.12   	t	t	f	462
755	0828.02   	t	t	f	462
765	0831.06   	t	t	f	462
783	0855.02   	t	t	f	462
796	0858.03   	t	t	f	462
821	0876.03   	t	t	f	462
845	0888.00   	t	t	f	462
857	0903.06   	t	t	f	462
869	0929.00   	t	t	f	462
291	0273.00   	t	t	t	462
292	0274.00   	t	t	t	462
293	0275.00   	t	t	t	462
294	0276.00   	t	t	t	462
295	0277.00   	t	t	t	462
296	0278.00   	t	t	t	462
297	0279.00   	t	t	t	462
298	0280.00   	t	t	t	462
299	0281.00   	t	t	t	462
300	0282.00   	t	t	t	462
301	0283.01   	t	t	t	462
302	0283.02   	t	t	t	462
303	0284.00   	t	t	t	462
304	0285.00   	t	t	t	462
305	0286.00   	t	t	t	462
306	0287.01   	t	t	t	462
307	0287.02   	t	t	t	462
308	0288.00   	t	t	t	462
309	0290.01   	t	t	t	462
310	0290.02   	t	t	t	462
311	0290.03   	t	t	t	462
312	0290.04   	t	t	t	462
313	0290.05   	t	t	t	462
314	0290.06   	t	t	t	462
315	0290.07   	t	t	t	462
316	0290.08   	t	t	t	462
317	0290.09   	t	t	t	462
318	0291.01   	t	t	t	462
320	0300.00   	t	t	t	462
321	0301.00   	t	t	t	462
322	0302.00   	t	t	t	462
323	0303.00   	t	t	t	462
324	0304.00   	t	t	t	462
325	0305.00   	t	t	t	462
326	0306.00   	t	t	t	462
327	0307.00   	t	t	t	462
328	0308.00   	t	t	t	462
329	0309.00   	t	t	t	462
330	0310.00   	t	t	t	462
331	0311.00   	t	t	t	462
332	0312.00   	t	t	t	462
333	0313.00   	t	t	t	462
334	0314.00   	t	t	t	462
335	0315.00   	t	t	t	462
336	0316.00   	t	t	t	462
338	0317.02   	t	t	t	462
339	0320.00   	t	t	t	462
340	0321.00   	t	t	t	462
341	0322.02   	t	t	t	462
342	0322.03   	t	t	t	462
343	0322.04   	t	t	t	462
344	0323.00   	t	t	t	462
345	0324.01   	t	t	t	462
346	0324.02   	t	t	t	462
347	0325.01   	t	t	t	462
348	0325.02   	t	t	t	462
349	0325.03   	t	t	t	462
351	0326.01   	t	t	t	462
352	0326.02   	t	t	t	462
353	0326.03   	t	t	t	462
354	0327.00   	t	t	t	462
355	0328.00   	t	t	t	462
356	0329.00   	t	t	t	462
357	0330.00   	t	t	t	462
358	0340.00   	t	t	t	462
359	0350.00   	t	t	t	462
360	0351.00   	t	t	t	462
361	0352.00   	t	t	t	462
362	0353.00   	t	t	t	462
363	0354.00   	t	t	t	462
364	0355.00   	t	t	t	462
365	0356.00   	t	t	t	462
366	0360.00   	t	t	t	462
367	0361.00   	t	t	t	462
368	0362.00   	t	t	t	462
369	0363.00   	t	t	t	462
370	0364.00   	t	t	t	462
371	0365.00   	t	t	t	462
372	0366.00   	t	t	t	462
373	0367.00   	t	t	t	462
374	0370.00   	t	t	t	462
375	0380.00   	t	t	t	462
376	0381.00   	t	t	t	462
377	0382.01   	t	t	t	462
378	0382.02   	t	t	t	462
379	0383.01   	t	t	t	462
380	0383.02   	t	t	t	462
381	0385.00   	t	t	t	462
382	0390.00   	t	t	t	462
383	0391.00   	t	t	t	462
384	0392.00   	t	t	t	462
385	0393.00   	t	t	t	462
386	0394.00   	t	t	t	462
387	0395.01   	t	t	t	462
388	0395.02   	t	t	t	462
389	0395.03   	t	t	t	462
390	0396.00   	t	t	t	462
391	0397.00   	t	t	t	462
392	0400.00   	t	t	t	462
393	0401.00   	t	t	t	462
394	0402.00   	t	t	t	462
395	0403.00   	t	t	t	462
396	0404.00   	t	t	t	462
397	0410.01   	t	t	t	462
398	0410.02   	t	t	t	462
399	0410.03   	t	t	t	462
400	0412.00   	t	t	t	462
403	0415.03   	t	t	t	462
404	0415.04   	t	t	t	462
405	0416.01   	t	t	t	462
406	0416.02   	t	t	t	462
407	0417.01   	t	t	t	462
408	0417.02   	t	t	t	462
409	0418.00   	t	t	t	462
410	0419.00   	t	t	t	462
411	0420.00   	t	t	t	462
412	0421.01   	t	t	t	462
413	0421.02   	t	t	t	462
414	0430.00   	t	t	t	462
415	0431.00   	t	t	t	462
416	0432.00   	t	t	t	462
417	0433.00   	t	t	t	462
418	0440.00   	t	t	t	462
419	0450.00   	t	t	t	462
420	0451.00   	t	t	t	462
421	0452.00   	t	t	t	462
422	0453.01   	t	t	t	462
423	0453.02   	t	t	t	462
424	0460.00   	t	t	t	462
425	0461.00   	t	t	t	462
426	0462.01   	t	t	t	462
427	0462.02   	t	t	t	462
428	0470.01   	t	t	t	462
429	0470.03   	t	t	t	462
430	0470.04   	t	t	t	462
431	0470.05   	t	t	t	462
432	0480.00   	t	t	t	462
433	0490.00   	t	t	t	462
434	0491.00   	t	t	t	462
435	0500.00   	t	t	t	462
438	0512.02   	t	t	t	462
439	0512.03   	t	t	t	462
440	0512.04   	t	t	t	462
441	0513.01   	t	t	t	462
442	0513.02   	t	t	t	462
443	0514.01   	t	t	t	462
444	0514.02   	t	t	t	462
445	0515.01   	t	t	t	462
446	0515.02   	t	t	t	462
447	0515.03   	t	t	t	462
448	0515.04   	t	t	t	462
449	0520.02   	t	t	t	462
450	0520.03   	t	t	t	462
451	0520.04   	t	t	t	462
452	0521.01   	t	t	t	462
453	0521.04   	t	t	t	462
454	0521.05   	t	t	t	462
455	0521.06   	t	t	t	462
456	0521.07   	t	t	t	462
457	0522.01   	t	t	t	462
458	0522.02   	t	t	t	462
459	0523.00   	t	t	t	462
460	0530.00   	t	t	t	462
461	0540.00   	t	t	t	462
462	0550.02   	t	t	t	462
463	0550.03   	t	t	t	462
464	0550.04   	t	t	t	462
465	0570.00   	t	t	t	462
466	0580.01   	t	t	t	462
467	0580.02   	t	t	t	462
468	0580.03   	t	t	t	462
469	0581.01   	t	t	t	462
470	0581.02   	t	t	t	462
471	0582.01   	t	t	t	462
472	0582.02   	t	t	t	462
473	0583.00   	t	t	t	462
474	0584.00   	t	t	t	462
475	0585.01   	t	t	t	462
476	0585.02   	t	t	t	462
477	0590.01   	t	t	t	462
478	0590.02   	t	t	t	462
479	0591.01   	t	t	t	462
480	0591.02   	t	t	t	462
481	0592.00   	t	t	t	462
483	0594.01   	t	t	t	462
484	0594.02   	t	t	t	462
485	0600.01   	t	t	t	462
486	0600.02   	t	t	t	462
487	0600.03   	t	t	t	462
488	0601.01   	t	t	t	462
489	0601.02   	t	t	t	462
490	0602.01   	t	t	t	462
491	0602.02   	t	t	t	462
492	0603.01   	t	t	t	462
493	0603.02   	t	t	t	462
494	0603.03   	t	t	t	462
534	0632.02   	t	t	t	462
535	0632.03   	t	t	t	462
536	0632.04   	t	t	t	462
537	0633.00   	t	t	t	462
538	0634.00   	t	t	t	462
539	0635.00   	t	t	t	462
540	0636.00   	t	t	t	462
541	0637.01   	t	t	t	462
542	0637.02   	t	t	t	462
543	0638.01   	t	t	t	462
544	0638.02   	t	t	t	462
545	0638.03   	t	t	t	462
546	0638.04   	t	t	t	462
547	0639.00   	t	t	t	462
548	0640.00   	t	t	t	462
549	0641.01   	t	t	t	462
550	0641.02   	t	t	t	462
551	0642.00   	t	t	t	462
553	0644.00   	t	t	t	462
554	0645.00   	t	t	t	462
555	0646.01   	t	t	t	462
556	0646.02   	t	t	t	462
557	0646.03   	t	t	t	462
558	0647.01   	t	t	t	462
559	0647.02   	t	t	t	462
560	0648.00   	t	t	t	462
561	0649.01   	t	t	t	462
562	0649.02   	t	t	t	462
563	0650.01   	t	t	t	462
564	0650.02   	t	t	t	462
565	0650.03   	t	t	t	462
566	0651.01   	t	t	t	462
567	0651.02   	t	t	t	462
568	0652.01   	t	t	t	462
573	0653.00   	t	t	t	462
574	0654.00   	t	t	t	462
575	0655.01   	t	t	t	462
576	0655.02   	t	t	t	462
577	0656.01   	t	t	t	462
579	0657.01   	t	t	t	462
580	0657.02   	t	t	t	462
581	0657.03   	t	t	t	462
582	0658.01   	t	t	t	462
583	0658.02   	t	t	t	462
584	0658.03   	t	t	t	462
586	0659.04   	t	t	t	462
587	0659.05   	t	t	t	462
588	0659.06   	t	t	t	462
590	0660.01   	t	t	t	462
591	0660.02   	t	t	t	462
592	0660.03   	t	t	t	462
593	0661.01   	t	t	t	462
595	0662.00   	t	t	t	462
596	0675.00   	t	t	t	462
597	0676.01   	t	t	t	462
1	0001.00   	t	t	t	462
2	0002.00   	t	t	t	462
3	0003.00   	t	t	t	462
4	0004.00   	t	t	t	462
5	0005.00   	t	t	t	462
6	0006.00   	t	t	t	462
7	0007.00   	t	t	t	462
8	0008.00   	t	t	t	462
9	0009.00   	t	t	t	462
10	0010.00   	t	t	t	462
11	0011.00   	t	t	t	462
12	0012.01   	t	t	t	462
13	0012.02   	t	t	t	462
14	0013.00   	t	t	t	462
15	0014.01   	t	t	t	462
16	0014.02   	t	t	t	462
17	0015.00   	t	t	t	462
18	0016.00   	t	t	t	462
19	0017.00   	t	t	t	462
20	0018.00   	t	t	t	462
21	0019.00   	t	t	t	462
22	0021.00   	t	t	t	462
23	0022.00   	t	t	t	462
24	0023.00   	t	t	t	462
25	0024.00   	t	t	t	462
26	0025.00   	t	t	t	462
27	0026.00   	t	t	t	462
28	0027.00   	t	t	t	462
29	0028.00   	t	t	t	462
30	0029.00   	t	t	t	462
31	0030.00   	t	t	t	462
32	0031.00   	t	t	t	462
33	0032.00   	t	t	t	462
34	0033.00   	t	t	t	462
35	0034.00   	t	t	t	462
36	0035.00   	t	t	t	462
37	0036.00   	t	t	t	462
38	0037.00   	t	t	t	462
39	0038.00   	t	t	t	462
598	0676.02   	t	t	t	462
599	0676.03   	t	t	t	462
600	0676.04   	t	t	t	462
601	0677.01   	t	t	t	462
602	0677.02   	t	t	t	462
604	0677.05   	t	t	t	462
605	0677.06   	t	t	t	462
606	0677.07   	t	t	t	462
607	0677.08   	t	t	t	462
608	0681.00   	t	t	t	462
609	0682.02   	t	t	t	462
610	0682.03   	t	t	t	462
611	0682.04   	t	t	t	462
612	0682.05   	t	t	t	462
613	0682.06   	t	t	t	462
614	0683.00   	t	t	t	462
615	0684.03   	t	t	t	462
617	0684.05   	t	t	t	462
619	0685.02   	t	t	t	462
620	0685.03   	t	t	t	462
621	0685.04   	t	t	t	462
622	0685.05   	t	t	t	462
623	0685.06   	t	t	t	462
624	0686.01   	t	t	t	462
625	0686.02   	t	t	t	462
626	0686.03   	t	t	t	462
627	0687.01   	t	t	t	462
629	0687.03   	t	t	t	462
630	0687.04   	t	t	t	462
631	0687.06   	t	t	t	462
632	0687.07   	t	t	t	462
633	0688.02   	t	t	t	462
636	0689.01   	t	t	t	462
637	0689.02   	t	t	t	462
638	0689.03   	t	t	t	462
639	0689.04   	t	t	t	462
640	0690.00   	t	t	t	462
641	0691.00   	t	t	t	462
642	0692.00   	t	t	t	462
643	0693.00   	t	t	t	462
644	0694.00   	t	t	t	462
645	0700.01   	t	t	t	462
646	0700.03   	t	t	t	462
647	0700.04   	t	t	t	462
649	0702.01   	t	t	t	462
650	0702.02   	t	t	t	462
651	0703.02   	t	t	t	462
652	0703.03   	t	t	t	462
653	0703.04   	t	t	t	462
654	0704.01   	t	t	t	462
655	0704.04   	t	t	t	462
656	0704.05   	t	t	t	462
657	0704.06   	t	t	t	462
660	0705.01   	t	t	t	462
661	0705.02   	t	t	t	462
662	0706.00   	t	t	t	462
663	0707.00   	t	t	t	462
664	0708.01   	t	t	t	462
665	0708.02   	t	t	t	462
666	0709.01   	t	t	t	462
667	0709.02   	t	t	t	462
670	0710.03   	t	t	t	462
672	0725.03   	t	t	t	462
673	0725.04   	t	t	t	462
674	0725.06   	t	t	t	462
677	0726.01   	t	t	t	462
678	0726.03   	t	t	t	462
679	0726.04   	t	t	t	462
680	0727.01   	t	t	t	462
681	0727.02   	t	t	t	462
682	0728.01   	t	t	t	462
683	0728.02   	t	t	t	462
684	0728.03   	t	t	t	462
685	0729.00   	t	t	t	462
686	0730.01   	t	t	t	462
687	0730.02   	t	t	t	462
688	0731.00   	t	t	t	462
689	0732.01   	t	t	t	462
690	0732.02   	t	t	t	462
691	0733.00   	t	t	t	462
692	0734.01   	t	t	t	462
694	0735.01   	t	t	t	462
695	0735.02   	t	t	t	462
697	0750.01   	t	t	t	462
698	0750.02   	t	t	t	462
700	0751.01   	t	t	t	462
701	0751.02   	t	t	t	462
704	0756.02   	t	t	t	462
705	0756.03   	t	t	t	462
706	0756.04   	t	t	t	462
707	0756.05   	t	t	t	462
708	0757.00   	t	t	t	462
711	0760.00   	t	t	t	462
712	0775.00   	t	t	t	462
713	0776.00   	t	t	t	462
715	0780.00   	t	t	t	462
716	0781.00   	t	t	t	462
717	0782.00   	t	t	t	462
718	0783.00   	t	t	t	462
719	0784.00   	t	t	t	462
720	0785.00   	t	t	t	462
721	0786.00   	t	t	t	462
722	0787.00   	t	t	t	462
723	0788.00   	t	t	t	462
724	0789.00   	t	t	t	462
725	0790.00   	t	t	t	462
726	0791.00   	t	t	t	462
727	0792.00   	t	t	t	462
728	0800.01   	t	t	t	462
729	0800.02   	t	t	t	462
730	0801.00   	t	t	t	462
731	0802.00   	t	t	t	462
733	0805.00   	t	t	t	462
734	0806.01   	t	t	t	462
735	0806.02   	t	t	t	462
736	0807.01   	t	t	t	462
737	0807.02   	t	t	t	462
738	0825.01   	t	t	t	462
739	0825.02   	t	t	t	462
740	0825.03   	t	t	t	462
741	0825.04   	t	t	t	462
742	0825.05   	t	t	t	462
743	0826.02   	t	t	t	462
744	0826.05   	t	t	t	462
745	0826.06   	t	t	t	462
746	0826.07   	t	t	t	462
747	0826.08   	t	t	t	462
748	0826.09   	t	t	t	462
749	0826.10   	t	t	t	462
750	0826.11   	t	t	t	462
752	0827.02   	t	t	t	462
753	0827.03   	t	t	t	462
754	0827.04   	t	t	t	462
756	0828.03   	t	t	t	462
757	0828.04   	t	t	t	462
758	0829.00   	t	t	t	462
759	0830.01   	t	t	t	462
762	0831.03   	t	t	t	462
763	0831.04   	t	t	t	462
764	0831.05   	t	t	t	462
766	0831.07   	t	t	t	462
767	0832.00   	t	t	t	462
768	0833.00   	t	t	t	462
769	0834.00   	t	t	t	462
770	0850.01   	t	t	t	462
771	0850.03   	t	t	t	462
772	0850.04   	t	t	t	462
773	0850.05   	t	t	t	462
774	0851.00   	t	t	t	462
775	0852.01   	t	t	t	462
776	0852.02   	t	t	t	462
777	0853.02   	t	t	t	462
778	0853.03   	t	t	t	462
779	0853.04   	t	t	t	462
780	0854.01   	t	t	t	462
781	0854.02   	t	t	t	462
782	0855.01   	t	t	t	462
784	0856.00   	t	t	t	462
785	0857.01   	t	t	t	462
786	0857.02   	t	t	t	462
787	0857.06   	t	t	t	462
788	0857.07   	t	t	t	462
789	0857.08   	t	t	t	462
790	0857.09   	t	t	t	462
791	0857.10   	t	t	t	462
792	0857.11   	t	t	t	462
793	0857.12   	t	t	t	462
794	0858.01   	t	t	t	462
795	0858.02   	t	t	t	462
797	0859.00   	t	t	t	462
798	0860.01   	t	t	t	462
799	0860.02   	t	t	t	462
800	0861.00   	t	t	t	462
801	0862.00   	t	t	t	462
802	0863.01   	t	t	t	462
803	0863.02   	t	t	t	462
804	0864.00   	t	t	t	462
805	0865.00   	t	t	t	462
806	0866.00   	t	t	t	462
807	0867.00   	t	t	t	462
808	0868.01   	t	t	t	462
40	0039.00   	t	t	t	462
41	0040.00   	t	t	t	462
42	0041.00   	t	t	t	462
43	0042.00   	t	t	t	462
44	0043.00   	t	t	t	462
45	0044.00   	t	t	t	462
46	0045.00   	t	t	t	462
47	0046.00   	t	t	t	462
48	0047.00   	t	t	t	462
49	0048.00   	t	t	t	462
50	0049.00   	t	t	t	462
51	0050.00   	t	t	t	462
52	0051.00   	t	t	t	462
53	0052.00   	t	t	t	462
54	0053.00   	t	t	t	462
55	0054.00   	t	t	t	462
56	0055.01   	t	t	t	462
57	0055.02   	t	t	t	462
58	0056.00   	t	t	t	462
59	0057.00   	t	t	t	462
60	0058.00   	t	t	t	462
61	0059.00   	t	t	t	462
62	0060.00   	t	t	t	462
63	0061.00   	t	t	t	462
64	0062.00   	t	t	t	462
65	0063.00   	t	t	t	462
66	0064.00   	t	t	t	462
67	0065.01   	t	t	t	462
68	0065.02   	t	t	t	462
69	0066.01   	t	t	t	462
70	0066.02   	t	t	t	462
71	0067.00   	t	t	t	462
72	0068.00   	t	t	t	462
73	0069.00   	t	t	t	462
74	0070.00   	t	t	t	462
75	0071.00   	t	t	t	462
76	0072.00   	t	t	t	462
77	0073.00   	t	t	t	462
78	0074.00   	t	t	t	462
79	0075.00   	t	t	t	462
80	0076.00   	t	t	t	462
81	0077.00   	t	t	t	462
82	0078.00   	t	t	t	462
83	0079.00   	t	t	t	462
84	0080.00   	t	t	t	462
85	0081.00   	t	t	t	462
86	0082.00   	t	t	t	462
87	0083.00   	t	t	t	462
88	0084.00   	t	t	t	462
89	0085.00   	t	t	t	462
90	0086.00   	t	t	t	462
91	0087.00   	t	t	t	462
92	0088.00   	t	t	t	462
93	0089.00   	t	t	t	462
94	0090.00   	t	t	t	462
95	0091.00   	t	t	t	462
96	0092.00   	t	t	t	462
97	0093.00   	t	t	t	462
98	0094.01   	t	t	t	462
99	0094.02   	t	t	t	462
100	0095.00   	t	t	t	462
101	0096.00   	t	t	t	462
102	0097.01   	t	t	t	462
103	0097.02   	t	t	t	462
104	0098.00   	t	t	t	462
105	0099.00   	t	t	t	462
107	0101.01   	t	t	t	462
108	0101.02   	t	t	t	462
109	0102.00   	t	t	t	462
110	0103.00   	t	t	t	462
111	0104.00   	t	t	t	462
112	0105.00   	t	t	t	462
113	0106.00   	t	t	t	462
114	0107.00   	t	t	t	462
115	0108.00   	t	t	t	462
116	0109.00   	t	t	t	462
117	0110.00   	t	t	t	462
118	0111.00   	t	t	t	462
106	0100.00   	t	t	f	462
809	0868.02   	t	t	t	462
810	0869.00   	t	t	t	462
811	0870.01   	t	t	t	462
812	0870.02   	t	t	t	462
813	0871.01   	t	t	t	462
814	0871.02   	t	t	t	462
815	0872.00   	t	t	t	462
816	0873.01   	t	t	t	462
817	0873.02   	t	t	t	462
818	0874.00   	t	t	t	462
819	0875.00   	t	t	t	462
820	0876.01   	t	t	t	462
822	0876.04   	t	t	t	462
823	0876.05   	t	t	t	462
824	0877.01   	t	t	t	462
825	0877.02   	t	t	t	462
826	0878.00   	t	t	t	462
827	0879.01   	t	t	t	462
828	0879.02   	t	t	t	462
829	0880.00   	t	t	t	462
830	0881.01   	t	t	t	462
831	0881.02   	t	t	t	462
832	0882.00   	t	t	t	462
833	0883.00   	t	t	t	462
834	0884.01   	t	t	t	462
835	0884.02   	t	t	t	462
836	0885.00   	t	t	t	462
837	0886.01   	t	t	t	462
838	0886.02   	t	t	t	462
839	0886.03   	t	t	t	462
840	0887.03   	t	t	t	462
841	0887.04   	t	t	t	462
846	0889.01   	t	t	t	462
847	0889.02   	t	t	t	462
848	0889.03   	t	t	t	462
849	0900.00   	t	t	t	462
850	0901.01   	t	t	t	462
851	0901.02   	t	t	t	462
852	0901.03   	t	t	t	462
853	0902.00   	t	t	t	462
854	0903.01   	t	t	t	462
855	0903.04   	t	t	t	462
856	0903.05   	t	t	t	462
858	0903.07   	t	t	t	462
859	0903.08   	t	t	t	462
860	0903.09   	t	t	t	462
861	0903.10   	t	t	t	462
862	0904.02   	t	t	t	462
863	0904.03   	t	t	t	462
864	0904.04   	t	t	t	462
865	0904.05   	t	t	t	462
866	0904.06   	t	t	t	462
867	0926.01   	t	t	t	462
868	0926.02   	t	t	t	462
870	0930.02   	t	t	t	462
871	0930.03   	t	t	t	462
872	0930.04   	t	t	t	462
204	0190.02   	t	t	t	462
205	0191.00   	t	t	t	462
206	0192.00   	t	t	t	462
207	0193.00   	t	t	t	462
208	0194.00   	t	t	t	462
209	0195.01   	t	t	t	462
210	0195.02   	t	t	t	462
119	0112.01   	t	t	t	462
120	0112.02   	t	t	t	462
121	0113.00   	t	t	t	462
122	0114.00   	t	t	t	462
123	0115.01   	t	t	t	462
124	0115.02   	t	t	t	462
125	0116.00   	t	t	t	462
126	0117.00   	t	t	t	462
127	0118.00   	t	t	t	462
128	0119.00   	t	t	t	462
130	0121.00   	t	t	t	462
131	0122.00   	t	t	t	462
132	0123.00   	t	t	t	462
133	0124.00   	t	t	t	462
134	0125.00   	t	t	t	462
135	0126.00   	t	t	t	462
136	0127.01   	t	t	t	462
137	0127.02   	t	t	t	462
138	0128.00   	t	t	t	462
139	0129.01   	t	t	t	462
140	0129.02   	t	t	t	462
141	0130.00   	t	t	t	462
142	0131.00   	t	t	t	462
143	0132.00   	t	t	t	462
144	0133.00   	t	t	t	462
145	0134.00   	t	t	t	462
146	0135.00   	t	t	t	462
147	0136.00   	t	t	t	462
148	0137.00   	t	t	t	462
149	0138.00   	t	t	t	462
150	0139.00   	t	t	t	462
151	0140.00   	t	t	t	462
152	0141.00   	t	t	t	462
153	0142.00   	t	t	t	462
154	0143.00   	t	t	t	462
155	0144.00   	t	t	t	462
156	0145.00   	t	t	t	462
157	0146.00   	t	t	t	462
158	0147.00   	t	t	t	462
159	0148.00   	t	t	t	462
160	0149.00   	t	t	t	462
161	0150.00   	t	t	t	462
162	0151.00   	t	t	t	462
163	0152.00   	t	t	t	462
164	0153.00   	t	t	t	462
165	0154.00   	t	t	t	462
166	0155.00   	t	t	t	462
167	0156.00   	t	t	t	462
168	0157.00   	t	t	t	462
169	0158.00   	t	t	t	462
170	0159.00   	t	t	t	462
171	0160.00   	t	t	t	462
172	0161.00   	t	t	t	462
173	0162.00   	t	t	t	462
174	0163.00   	t	t	t	462
175	0164.00   	t	t	t	462
176	0165.00   	t	t	t	462
177	0166.00   	t	t	t	462
211	0195.03   	t	t	t	462
212	0196.00   	t	t	t	462
213	0197.00   	t	t	t	462
214	0198.00   	t	t	t	462
215	0199.00   	t	t	t	462
216	0200.00   	t	t	t	462
217	0201.00   	t	t	t	462
218	0202.00   	t	t	t	462
219	0203.00   	t	t	t	462
220	0204.00   	t	t	t	462
221	0205.00   	t	t	t	462
222	0206.00   	t	t	t	462
223	0207.00   	t	t	t	462
224	0208.00   	t	t	t	462
225	0209.00   	t	t	t	462
226	0210.00   	t	t	t	462
227	0211.00   	t	t	t	462
228	0212.00   	t	t	t	462
229	0213.00   	t	t	t	462
230	0214.00   	t	t	t	462
231	0215.00   	t	t	t	462
232	0216.00   	t	t	t	462
233	0217.00   	t	t	t	462
234	0218.00   	t	t	t	462
235	0219.00   	t	t	t	462
236	0220.00   	t	t	t	462
237	0221.00   	t	t	t	462
238	0222.00   	t	t	t	462
239	0223.01   	t	t	t	462
240	0223.02   	t	t	t	462
241	0224.00   	t	t	t	462
242	0225.00   	t	t	t	462
243	0226.00   	t	t	t	462
244	0227.00   	t	t	t	462
245	0228.00   	t	t	t	462
246	0229.00   	t	t	t	462
247	0230.00   	t	t	t	462
248	0231.00   	t	t	t	462
249	0232.00   	t	t	t	462
250	0233.00   	t	t	t	462
251	0234.00   	t	t	t	462
252	0235.00   	t	t	t	462
253	0236.00   	t	t	t	462
254	0237.00   	t	t	t	462
255	0238.00   	t	t	t	462
256	0239.00   	t	t	t	462
257	0240.00   	t	t	t	462
258	0241.00   	t	t	t	462
259	0242.00   	t	t	t	462
260	0243.00   	t	t	t	462
261	0244.00   	t	t	t	462
178	0167.00   	t	t	t	462
179	0168.00   	t	t	t	462
180	0169.00   	t	t	t	462
181	0170.00   	t	t	t	462
182	0171.00   	t	t	t	462
183	0172.00   	t	t	t	462
184	0173.00   	t	t	t	462
185	0174.00   	t	t	t	462
186	0175.00   	t	t	t	462
187	0176.00   	t	t	t	462
188	0177.00   	t	t	t	462
189	0178.00   	t	t	t	462
190	0179.00   	t	t	t	462
191	0180.00   	t	t	t	462
192	0181.00   	t	t	t	462
193	0182.00   	t	t	t	462
194	0183.00   	t	t	t	462
195	0184.00   	t	t	t	462
196	0185.00   	t	t	t	462
197	0186.00   	t	t	t	462
198	0187.01   	t	t	t	462
200	0188.01   	t	t	t	462
201	0188.02   	t	t	t	462
202	0189.00   	t	t	t	462
203	0190.01   	t	t	t	462
129	0120.00   	t	t	f	462
199	0187.02   	t	t	f	462
570	0652.05   	f	t	f	462
436	0511.01   	f	t	t	462
437	0511.02   	f	t	t	462
569	0652.04   	f	t	t	462
571	0652.06   	f	t	t	462
572	0652.07   	f	t	t	462
634	0688.03   	f	t	t	462
635	0688.04   	f	t	t	462
658	0704.08   	f	t	t	462
659	0704.09   	f	t	t	462
675	0725.07   	f	t	t	462
676	0725.08   	f	t	t	462
702	0755.01   	f	t	t	462
703	0755.02   	f	t	t	462
760	0830.03   	f	t	t	462
761	0830.04   	f	t	t	462
879	0100.01   	f	f	t	462
880	0100.02   	f	f	t	462
881	0120.01   	f	f	t	462
882	0120.02   	f	f	t	462
883	0187.03   	f	f	t	462
884	0187.04   	f	f	t	462
885	0291.03   	f	f	t	462
886	0291.04   	f	f	t	462
887	0317.03   	f	f	t	462
888	0317.04   	f	f	t	462
889	0325.05   	f	f	t	462
890	0325.06   	f	f	t	462
891	0413.01   	f	f	t	462
892	0413.02   	f	f	t	462
893	0415.05   	f	f	t	462
894	0415.06   	f	f	t	462
262	0245.00   	t	t	t	462
263	0247.00   	t	t	t	462
264	0249.00   	t	t	t	462
265	0250.00   	t	t	t	462
266	0251.01   	t	t	t	462
267	0251.02   	t	t	t	462
268	0252.00   	t	t	t	462
269	0253.00   	t	t	t	462
270	0254.00   	t	t	t	462
271	0256.00   	t	t	t	462
272	0257.00   	t	t	t	462
273	0258.00   	t	t	t	462
274	0259.00   	t	t	t	462
275	0260.00   	t	t	t	462
276	0261.00   	t	t	t	462
277	0262.00   	t	t	t	462
278	0263.00   	t	t	t	462
279	0264.01   	t	t	t	462
280	0264.02   	t	t	t	462
281	0265.00   	t	t	t	462
282	0266.00   	t	t	t	462
283	0267.00   	t	t	t	462
284	0268.01   	t	t	t	462
285	0268.02   	t	t	t	462
286	0268.03   	t	t	t	462
287	0269.00   	t	t	t	462
288	0270.00   	t	t	t	462
289	0271.00   	t	t	t	462
290	0272.00   	t	t	t	462
319	0291.02   	t	t	f	462
337	0317.01   	t	t	f	462
350	0325.04   	t	t	f	462
401	0413.00   	t	t	f	462
402	0415.01   	t	t	f	462
482	0593.00   	t	t	f	462
495	0604.01   	t	t	t	462
496	0604.02   	t	t	t	462
497	0604.03   	t	t	t	462
498	0604.04   	t	t	t	462
499	0604.05   	t	t	t	462
500	0605.01   	t	t	t	462
501	0605.02   	t	t	t	462
502	0605.03   	t	t	t	462
503	0605.04   	t	t	t	462
504	0605.05   	t	t	t	462
505	0610.01   	t	t	t	462
506	0610.02   	t	t	t	462
507	0610.03   	t	t	t	462
508	0610.04   	t	t	t	462
509	0610.05   	t	t	t	462
510	0610.06   	t	t	t	462
511	0610.07   	t	t	t	462
512	0611.01   	t	t	t	462
513	0611.02   	t	t	t	462
514	0612.00   	t	t	t	462
515	0613.00   	t	t	t	462
516	0614.00   	t	t	t	462
517	0615.00   	t	t	t	462
518	0616.00   	t	t	t	462
519	0617.01   	t	t	t	462
520	0617.02   	t	t	t	462
521	0618.00   	t	t	t	462
522	0619.00   	t	t	t	462
523	0625.01   	t	t	t	462
524	0625.02   	t	t	t	462
525	0626.00   	t	t	t	462
895	0593.01   	f	f	t	462
896	0593.02   	f	f	t	462
897	0643.01   	f	f	t	462
898	0643.02   	f	f	t	462
899	0643.03   	f	f	t	462
900	0652.08   	f	f	t	462
901	0652.09   	f	f	t	462
902	0656.03   	f	f	t	462
903	0656.04   	f	f	t	462
904	0659.08   	f	f	t	462
905	0659.09   	f	f	t	462
906	0659.10   	f	f	t	462
907	0659.11   	f	f	t	462
908	0661.03   	f	f	t	462
909	0661.04   	f	f	t	462
910	0677.09   	f	f	t	462
911	0677.10   	f	f	t	462
912	0684.08   	f	f	t	462
913	0684.09   	f	f	t	462
914	0684.10   	f	f	t	462
915	0684.11   	f	f	t	462
916	0687.09   	f	f	t	462
917	0687.10   	f	f	t	462
918	0701.01   	f	f	t	462
919	0701.02   	f	f	t	462
920	0710.05   	f	f	t	462
921	0710.06   	f	f	t	462
922	0710.07   	f	f	t	462
923	0710.08   	f	f	t	462
924	0710.09   	f	f	t	462
925	0710.10   	f	f	t	462
926	0710.11   	f	f	t	462
927	0734.03   	f	f	t	462
928	0734.04   	f	f	t	462
929	0734.05   	f	f	t	462
930	0740.01   	f	f	t	462
931	0740.02   	f	f	t	462
932	0750.04   	f	f	t	462
933	0750.05   	f	f	t	462
934	0758.03   	f	f	t	462
935	0758.04   	f	f	t	462
936	0758.05   	f	f	t	462
937	0758.06   	f	f	t	462
938	0777.01   	f	f	t	462
939	0777.02   	f	f	t	462
940	0804.01   	f	f	t	462
941	0804.02   	f	f	t	462
942	0826.13   	f	f	t	462
943	0826.14   	f	f	t	462
944	0828.05   	f	f	t	462
945	0828.06   	f	f	t	462
946	0831.08   	f	f	t	462
947	0831.09   	f	f	t	462
948	0855.03   	f	f	t	462
949	0855.04   	f	f	t	462
950	0858.04   	f	f	t	462
951	0858.05   	f	f	t	462
842	0887.05   	f	t	t	462
843	0887.06   	f	t	t	462
844	0887.07   	f	t	t	462
873	1000.00   	f	t	t	462
874	1001.00   	f	t	t	462
875	1002.00   	f	t	t	462
876	1003.00   	f	t	t	462
877	1004.00   	f	t	t	462
878	1005.00   	f	t	t	462
961	0652.03   	t	f	f	462
962	0511.00   	t	f	f	462
963	0830.02   	t	f	f	462
964	0725.05   	t	f	f	462
965	0887.02   	t	f	f	462
966	0688.01   	t	f	f	462
967	0755.00   	t	f	f	462
968	0652.02   	t	f	f	462
969	0704.03   	t	f	f	462
952	0876.06   	f	f	t	462
953	0876.07   	f	f	t	462
954	0888.01   	f	f	t	462
955	0888.02   	f	f	t	462
956	0888.03   	f	f	t	462
957	0903.11   	f	f	t	462
958	0903.12   	f	f	t	462
959	0929.01   	f	f	t	462
960	0929.02   	f	f	t	462
\.


--
-- Data for Name: g_ct2clsc; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct2clsc (src, dst) FROM stdin;
1	6043
2	6043
3	6043
4	6043
5	6043
6	6043
7	6043
8	6043
9	6051
10	6051
11	6051
12	6051
13	6051
14	6051
15	6052
16	6052
17	6052
18	6052
19	6052
20	6052
21	6052
22	6052
23	6052
24	6052
25	6052
26	6052
27	6052
28	6052
29	6052
30	6052
31	6052
32	6052
33	6092
34	6092
35	6092
36	6092
37	6092
38	6092
39	6092
40	6092
41	6034
42	6092
43	6092
44	6092
45	6092
46	6092
47	6092
48	6092
49	6092
50	6092
51	6092
52	6092
53	6092
54	6092
55	6094
56	6094
57	6034
58	6094
59	6094
60	6094
61	6094
62	6094
63	6074
64	6094
65	6074
66	6094
67	6074
68	6074
69	6074
70	6074
71	6034
72	6034
73	6034
74	6034
75	6034
76	6031
77	6031
78	6031
79	6031
80	6031
81	6034
82	6034
83	6034
84	6034
85	6034
86	6034
87	6034
88	6034
89	6033
90	6033
91	6033
92	6033
93	6033
94	6033
95	6033
96	6033
97	6033
98	6082
99	6034
100	6082
101	6082
102	6082
103	6082
104	6082
105	6082
106	6082
107	6082
108	6082
109	6082
110	6082
111	6082
112	6082
113	6082
114	6082
115	6082
116	6072
117	6081
118	6081
119	6072
120	6072
121	6072
122	6072
123	6072
124	6072
125	6071
126	6072
127	6072
128	6071
129	6072
130	6073
131	6071
132	6071
133	6071
134	6071
135	6071
136	6071
137	6071
138	6074
139	6074
140	6074
141	6074
142	6074
143	6074
144	6091
145	6091
146	6091
147	6091
148	6091
149	6091
150	6091
151	6093
152	6093
153	6093
154	6092
155	6092
156	6092
157	6093
158	6093
159	6093
160	6092
161	6093
162	6093
163	6093
164	6093
165	6093
166	6093
167	6093
168	6093
169	6093
170	6093
171	6093
172	6091
173	6091
174	6091
175	6091
176	6091
177	6091
178	6091
179	6091
180	6091
181	6091
182	6091
183	6093
184	6093
185	6093
186	6093
187	6093
188	6093
189	6093
190	6122
191	6053
192	6053
193	6053
194	6053
195	6053
196	6053
197	6053
198	6052
199	6053
200	6053
201	6053
202	6053
203	6051
204	6053
205	6051
206	6043
207	6043
208	6043
209	6053
210	6051
211	6051
212	6053
213	6053
214	6053
215	6053
216	6053
217	6053
218	6053
219	6053
220	6053
221	6122
222	6122
223	6122
224	6122
225	6122
226	6122
227	6122
228	6122
229	6122
230	6122
231	6122
232	6122
233	6122
234	6122
235	6122
236	6075
237	6075
238	6075
239	6075
240	6075
241	6075
242	6121
243	6121
244	6121
245	6121
246	6121
247	6121
248	6121
249	6121
250	6121
251	6121
252	6121
253	6121
254	6121
255	6121
256	6121
257	6121
258	6121
259	6121
260	6121
261	6121
262	6121
263	6121
264	6121
265	6062
266	6062
267	6062
268	6062
269	6062
270	6062
271	6062
272	6062
273	6062
274	6062
275	6062
276	6062
277	6062
278	6062
279	6132
280	6132
281	6132
282	6132
283	6132
284	6111
285	6111
286	6111
287	6132
288	6132
289	6132
290	6132
291	6132
292	6132
293	6132
294	6132
295	6132
296	6132
297	6132
298	6132
299	6132
300	6111
301	6111
302	6111
303	6111
304	6111
305	6111
306	6111
307	6111
308	6112
309	6041
310	6041
311	6041
312	6041
313	6041
314	6041
315	6041
316	6041
317	6041
318	6041
319	6041
320	6032
321	6032
322	6032
323	6032
324	6032
325	6032
326	6032
327	6032
328	6032
329	6032
330	6032
331	6032
332	6032
333	6032
334	6032
335	6032
336	6032
337	6032
338	6032
339	6022
340	6022
341	6022
342	6022
343	6022
344	6022
345	6022
346	6022
347	6022
348	6022
349	6022
350	6022
351	6022
352	6022
353	6022
354	6022
355	6022
356	6022
357	6021
358	6082
359	6074
360	6074
361	6074
362	6074
363	6074
364	6074
365	6074
366	6073
367	6073
368	6073
369	6073
370	6073
371	6073
372	6073
373	6073
374	6081
375	6081
376	6081
377	6081
378	6081
379	6081
380	6081
381	6081
382	6021
383	6021
384	6021
385	6021
386	6021
387	6021
388	6021
389	6021
390	6021
391	6021
392	6073
393	6073
394	6073
395	6073
396	6073
397	6112
398	6112
399	6112
400	6112
401	6112
402	6112
403	6112
404	6112
405	6111
406	6112
407	6112
408	6112
409	6112
410	6112
411	6112
412	6112
413	6112
414	6021
415	6021
416	6021
417	6021
418	6021
419	6011
420	6011
421	6011
422	6011
423	6011
424	6011
425	6011
426	6011
427	6011
428	6011
429	6011
430	6011
431	6011
432	6011
433	6011
434	6011
435	6011
436	6012
437	6012
438	6012
439	6012
440	6012
441	6012
442	6012
443	6012
444	6012
445	6012
446	6012
447	6012
448	6012
449	6013
450	6013
451	6013
452	6013
453	6013
454	6013
455	6013
456	6013
457	6013
458	6013
459	6013
460	6012
461	6012
462	6012
463	6012
464	6012
465	6042
466	6042
467	6042
468	6042
469	6042
470	6042
471	6042
472	6042
473	6042
474	6042
475	6042
476	6042
477	6044
478	6044
479	6044
480	6044
481	6044
482	6044
483	6044
484	6044
485	6061
486	6061
487	6061
488	6061
489	6061
490	6061
491	6061
492	6061
493	6061
494	6061
495	6061
496	6061
497	6061
498	6061
499	6061
500	6061
501	6061
502	6061
503	6061
504	6061
505	6131
506	6131
507	6131
508	6131
509	6131
510	6131
511	6131
512	6131
513	6131
514	6131
515	6131
516	6131
517	6131
518	6131
519	6131
520	6131
521	6131
522	6131
523	13011
524	13011
525	13011
526	13011
527	13011
528	13011
529	13011
530	13011
531	13011
532	13014
533	13013
534	13013
535	13013
536	13013
537	13013
538	13013
539	13013
540	13013
541	13013
542	13013
543	13013
544	13013
545	13013
546	13013
547	13013
548	13013
549	13012
550	13012
551	13012
552	13012
553	13012
554	13012
555	13012
556	13012
557	13012
558	13012
559	13012
560	13012
561	13012
562	13012
563	13012
564	13012
565	13012
566	13012
567	13012
568	13012
569	13012
570	13012
571	13014
572	13012
573	13012
574	13014
575	13014
576	13014
577	13014
578	13014
579	13014
580	13014
581	13014
582	13014
583	13014
584	13014
585	13014
586	13014
587	13014
588	13014
589	13014
590	13014
591	13014
592	13014
593	13014
594	13014
595	13011
596	14022
597	14022
598	14022
599	14022
600	14022
601	14022
602	14022
603	14022
604	14022
605	14022
606	14022
607	14022
608	14022
609	14022
610	14022
611	14022
612	14022
613	14022
614	14022
615	14021
616	14021
617	14021
618	14021
619	14021
620	14021
621	14021
622	14021
623	14021
624	14021
625	14021
626	14021
627	14021
628	14021
629	14021
630	14021
631	14021
632	14021
633	15031
634	15031
635	15031
636	14021
637	14021
638	14021
639	14021
640	14011
641	14011
642	14022
643	14022
644	14022
645	14021
646	14021
647	14021
648	15031
649	15031
650	15031
651	15031
652	15031
653	15031
654	15031
655	15031
656	15031
657	15031
658	15031
659	15031
660	15031
661	15031
662	15031
663	15031
664	15031
665	15031
666	15031
667	15031
668	15031
669	15031
670	15031
671	15031
672	15011
673	15011
674	15011
675	15011
676	15011
677	15011
678	15011
679	15011
680	15011
681	15011
682	15011
683	15011
684	15011
685	15011
686	15011
687	15011
688	15011
689	15011
690	15011
691	15021
692	15021
693	15021
694	15021
695	15021
696	15071
697	16121
698	16121
699	16121
700	16121
701	16121
702	16121
703	16121
704	16121
705	16121
706	16121
707	16121
708	16121
709	16121
710	16121
711	16121
712	16041
713	16031
714	16031
715	15021
716	15021
717	15021
718	15021
719	15021
720	15021
721	15021
722	15021
723	15021
724	15021
725	15021
726	15021
727	15071
728	16041
729	16041
730	16041
731	16041
732	16041
733	16041
734	16041
735	16041
736	16041
737	16041
738	16052
739	16052
740	16052
741	16052
742	16052
743	16052
744	16052
745	16052
746	16052
747	16052
748	16052
749	16052
750	16052
751	16052
752	16043
753	16043
754	16043
755	16043
756	16043
757	16043
758	16043
759	16043
760	16043
761	16043
762	16043
763	16043
764	16043
765	16043
766	16043
767	16041
768	16043
769	16041
770	16072
771	16072
772	16072
773	16072
774	16072
775	16072
776	16072
777	16072
778	16081
779	16081
780	16081
781	16081
782	16081
783	16081
784	16051
785	16051
786	16051
787	16051
788	16051
789	16051
790	16051
791	16051
792	16051
793	16051
794	16051
795	16051
796	16051
797	16052
798	16051
799	16051
800	16052
801	16052
802	16052
803	16052
804	16052
805	16052
806	16052
807	16052
808	16052
809	16052
810	16061
811	16061
812	16061
813	16061
814	16061
815	16061
816	16061
817	16061
818	16062
819	16061
820	16062
821	16062
822	16062
823	16062
824	16062
825	16062
826	16061
827	16061
828	16061
829	16061
830	16061
831	16061
832	16061
833	16061
834	16061
835	16062
836	16061
837	16062
838	16062
839	16062
840	16063
841	16063
842	16063
843	16063
844	16063
845	16063
846	16063
847	16063
848	16063
849	16081
850	16081
851	16081
852	16081
853	16081
854	16063
855	16063
856	16063
857	16063
858	16081
859	16063
860	16081
861	16063
862	16063
863	16063
864	16063
865	16063
866	16063
867	16072
868	16072
869	16081
870	16081
871	16081
872	16081
873	14022
874	14022
875	16063
876	16121
877	16121
878	16121
879	6082
880	6082
881	6072
882	6072
883	6053
884	6053
885	6041
886	6041
887	6032
888	6032
889	6022
890	6022
891	6112
892	6112
893	6112
894	6112
895	6044
896	6044
897	13012
898	13012
899	13012
900	13012
901	13012
902	13014
903	13014
904	13014
905	13014
906	13014
907	13014
908	13014
909	13014
910	14022
911	14022
912	14021
913	14021
914	14021
915	14021
916	14021
917	14021
918	15031
919	15031
920	15031
921	15031
922	15031
923	15031
924	15031
925	15031
926	15031
927	15011
928	15021
929	15011
930	15071
931	15021
932	16121
933	16121
934	16121
935	16121
936	16121
937	16121
938	16031
939	16031
940	16041
941	16041
942	16052
943	16052
944	16043
945	16043
946	16043
947	16043
948	16081
949	16081
950	16051
951	16051
952	16062
953	16062
954	16063
955	16063
956	16063
957	16063
958	16063
959	16081
960	16081
961	13012
962	6012
963	16043
964	15011
965	16063
966	15031
967	16121
968	13014
969	15031
\.


--
-- Data for Name: g_ct2neigh; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct2neigh (src, dst) FROM stdin;
1	60433
2	60433
3	60433
4	60435
5	60434
6	60434
7	60435
8	60434
9	60513
10	60512
11	60512
12	60512
13	60512
14	60512
15	60521
16	60521
17	60521
18	60521
19	60521
20	60521
21	60521
22	60522
23	60522
24	60522
25	60522
26	60522
27	60523
28	60523
29	60523
30	60523
31	60523
32	60523
33	60921
34	60921
35	60921
36	60921
37	60921
38	60921
39	60921
40	60921
41	60341
42	60922
43	60922
44	60922
45	60922
46	60922
47	60922
48	60922
49	60922
50	60922
51	60932
52	60924
53	60924
54	60924
55	60924
56	60923
57	60341
58	60924
59	60924
60	60924
61	60924
62	60924
63	60924
64	60924
65	60742
66	60924
67	60742
68	60742
69	60742
70	60742
71	60343
72	60343
73	60341
74	60341
75	60341
76	60311
77	60311
78	60311
79	60311
80	60311
81	60343
82	60343
83	60342
84	60342
85	60342
86	60342
87	60342
88	60342
89	60321
90	60321
91	60321
92	60321
93	60321
94	60325
95	60325
96	60325
97	60325
98	60822
99	60342
100	60822
101	60822
102	60822
103	60822
104	60825
105	60825
106	60824
107	60824
108	60824
109	60821
110	60821
111	60821
112	60821
113	60821
114	60821
115	60821
116	60713
117	60811
118	60811
119	60714
120	60714
121	60713
122	60713
123	60713
124	60713
125	60713
126	60714
127	60714
128	60714
129	60714
130	60711
131	60714
132	60714
133	60714
134	60714
135	60713
136	60713
137	60713
138	60742
139	60742
140	60742
141	60741
142	60741
143	60741
144	60911
145	60911
146	60911
147	60911
148	60911
149	60911
150	60911
151	60932
152	60932
153	60932
154	60932
155	60932
156	60932
157	60932
158	60932
159	60931
160	60932
161	60931
162	60931
163	60931
164	60931
165	60931
166	60931
167	60931
168	60932
169	60932
170	60932
171	60932
172	60912
173	60912
174	60912
175	60912
176	60912
177	60912
178	60912
179	60912
180	60912
181	60912
182	60912
183	60932
184	60932
185	60932
186	60932
187	60932
188	60931
189	60931
190	61221
191	60534
192	60534
193	60534
194	60534
195	60534
196	60534
197	60534
198	60523
199	60533
200	60532
201	60532
202	60532
203	60511
204	60532
205	60511
206	60436
207	60432
208	60432
209	60531
210	60511
211	60511
212	60531
213	60531
214	60531
215	60532
216	60531
217	60535
218	60535
219	60535
220	60535
221	61221
222	61221
223	61221
224	61221
225	61221
226	61222
227	61222
228	61222
229	61222
230	61222
231	61222
232	61222
233	61222
234	61222
235	61222
236	60751
237	60751
238	60751
239	60751
240	60751
241	60751
242	61213
243	61213
244	61213
245	61213
246	61213
247	61213
248	61213
249	61213
250	61212
251	61212
252	61212
253	61212
254	61213
255	61213
256	61212
257	61211
258	61211
259	61211
260	61211
261	61212
262	61212
263	61212
264	61211
265	60622
266	60622
267	60622
268	60622
269	60622
270	60622
271	60623
272	60623
273	60621
274	60621
275	60623
276	60623
277	60623
278	60623
279	61322
280	61322
281	61323
282	61323
283	61323
284	61112
285	61112
286	61112
287	61324
288	61325
289	61325
290	61321
291	61321
292	61321
293	61321
294	61321
295	61321
296	61325
297	61325
298	61324
299	61324
300	61111
301	61113
302	61113
303	61113
304	61113
305	61113
306	61113
307	61113
308	61113
309	60412
310	60412
311	60412
312	60411
313	60412
314	60412
315	60412
316	60412
317	60412
318	60411
319	60411
320	60322
321	60322
322	60322
323	60322
324	60322
325	60322
326	60322
327	60322
328	60322
329	60324
330	60324
331	60324
332	60324
333	60324
334	60324
335	60324
336	60324
337	60323
338	60323
339	60223
340	60223
341	60224
342	60224
343	60224
344	60224
345	60224
346	60224
347	60225
348	60222
349	60222
350	60222
351	60222
352	60225
353	60222
354	60225
355	60221
356	60226
357	60213
358	60823
359	60743
360	60743
361	60743
362	60743
363	60743
364	60743
365	60743
366	60712
367	60712
368	60712
369	60712
370	60712
371	60712
372	60712
373	60712
374	60812
375	60815
376	60813
377	60814
378	60814
379	60814
380	60814
381	60812
382	60214
383	60214
384	60214
385	60214
386	60214
387	60212
388	60214
389	60215
390	60215
391	60215
392	60711
393	60711
394	60711
395	60711
396	60711
397	61122
398	61123
399	61124
400	61124
401	61124
402	61125
403	61124
404	61124
405	61113
406	61113
407	61121
408	61121
409	61124
410	61121
411	61122
412	61123
413	61123
414	60211
415	60211
416	60211
417	60211
418	60211
419	60113
420	60113
421	60113
422	60112
423	60112
424	60114
425	60114
426	60114
427	60114
428	60111
429	60111
430	60111
431	60111
432	60115
433	60116
434	60116
435	60116
436	60125
437	60125
438	60122
439	60122
440	60122
441	60123
442	60123
443	60128
444	60128
445	60128
446	60128
447	60128
448	60128
449	60126
450	60126
451	60126
452	60127
453	60127
454	60127
455	60127
456	60127
457	60127
458	60127
459	60127
460	60124
461	60129
462	60121
463	60121
464	60121
465	60421
466	60422
467	60422
468	60422
469	60423
470	60423
471	60423
472	60423
473	60423
474	60423
475	60422
476	60423
477	60431
478	60431
479	60431
480	60431
481	60431
482	60437
483	60437
484	60437
485	60612
486	60612
487	60612
488	60612
489	60612
490	60612
491	60612
492	60611
493	60611
494	60611
495	60611
496	60611
497	60611
498	60611
499	60611
500	60613
501	60613
502	60613
503	60613
504	60613
505	61312
506	61312
507	61312
508	61311
509	61311
510	61312
511	61311
512	61312
513	61312
514	61312
515	61313
516	61312
517	61312
518	61312
519	61312
520	61312
521	61312
522	61312
523	72000
524	72000
525	72000
526	72000
527	71300
528	71300
529	71300
530	71300
531	71300
532	71000
533	70900
534	70900
535	70900
536	70900
537	71400
538	71400
539	71400
540	71400
541	70800
542	70800
543	70800
544	70800
545	70800
546	70800
547	70800
548	70600
549	70600
550	70600
551	70600
552	70500
553	70500
554	70500
555	70500
556	70500
557	70500
558	70400
559	70400
560	70400
561	70400
562	70400
563	70300
564	70300
565	70300
566	70300
567	70300
568	70100
569	70100
570	70100
571	70100
572	70100
573	70100
574	70100
575	70000
576	70000
577	70000
578	70000
579	70200
580	70200
581	70200
582	71500
583	71500
584	71500
585	70700
586	70700
587	70700
588	70700
589	70700
590	71100
591	71100
592	71100
593	71000
594	71000
595	71000
596	92200
597	92200
598	92200
599	92200
600	92200
601	92300
602	92300
603	92300
604	92300
605	92300
606	92300
607	92300
608	92100
609	92000
610	92000
611	92000
612	92000
613	92000
614	92000
615	91800
616	91800
617	91800
618	91800
619	91900
620	91700
621	91700
622	91700
623	91700
624	91600
625	91600
626	91600
627	91500
628	91410
629	91410
630	91500
631	91410
632	91410
633	91400
634	91400
635	91400
636	91600
637	91600
638	91600
640	92110
641	92110
642	92100
643	92100
644	92100
645	91800
646	91800
647	91800
648	91300
649	91300
650	91300
651	91300
652	91300
653	91300
654	90600
655	90600
656	90600
657	90600
658	90700
659	90700
660	90700
661	90700
662	90700
663	90700
664	91200
665	91200
666	90800
667	90800
668	90800
669	90800
670	90800
671	90800
672	90400
673	90400
674	90410
675	90410
676	90410
677	90400
678	90410
679	90400
680	90410
681	90410
682	90200
683	90200
684	90200
685	90200
686	90200
687	90200
688	90000
689	90000
690	90100
691	90300
692	90300
693	90300
694	90900
695	90900
696	90500
697	82700
698	82700
699	82700
700	82700
701	82700
702	82800
703	82800
704	82900
705	82800
706	82800
707	82800
708	82900
709	82900
710	82900
711	83000
712	81800
713	82600
714	82600
715	91000
716	91000
717	91100
718	91100
719	91000
720	91000
721	91000
722	91000
723	91100
724	91100
725	90500
726	91100
727	90500
728	81900
729	81900
730	81900
731	81800
732	81800
733	81900
734	81900
735	81900
736	82000
737	82000
738	80000
739	80100
740	80100
741	84000
742	84000
743	80000
744	80200
745	80200
746	80200
747	80200
748	80200
749	80200
750	80000
751	80000
752	82500
753	82500
754	82500
755	82400
756	82400
757	82400
758	82400
759	81500
760	81500
761	81500
762	82200
763	82200
764	82200
765	82300
766	82300
767	82100
768	82500
769	82000
770	83900
771	83900
772	83900
773	83900
774	83800
775	83900
776	83900
777	83800
778	83800
779	83800
780	83700
781	83700
782	83700
783	83700
784	80500
785	80500
786	80500
787	80600
788	80600
789	80600
790	80600
791	80600
792	80500
793	80500
794	80400
795	80400
796	80400
797	80100
798	80300
799	80300
800	80700
801	84000
802	84000
803	84000
804	80710
805	80710
806	80710
807	80700
808	80300
809	80300
810	80900
811	80900
812	80900
813	81000
814	81000
815	81000
816	81000
817	81000
818	81100
819	81100
820	80800
821	80800
822	80800
823	80800
824	81400
825	81400
826	81100
827	81100
828	81100
829	81200
830	81200
831	81200
832	81200
833	81300
834	81300
835	81300
836	81300
837	81400
838	81400
839	81400
840	83300
841	83300
842	83300
843	83300
844	83200
845	83200
846	83200
847	83200
848	83200
849	83500
850	83500
851	83500
852	83500
853	83500
854	83400
855	83400
856	83400
857	83400
858	83100
859	83100
860	83100
861	83100
862	83110
863	83110
864	83110
865	83110
866	83110
867	83800
868	83800
869	83600
870	83600
871	83600
872	83600
639	99999
873	99999
874	99999
875	99999
876	99999
877	99999
878	99999
879	60824
880	60824
881	60714
882	60714
883	60533
884	60533
885	60411
886	60411
887	60323
888	60323
889	60222
890	60222
891	61124
892	61124
893	61125
894	61125
895	60437
896	60437
897	70500
898	70500
899	70500
900	70100
901	70100
902	70000
903	70000
904	70700
905	70700
906	70700
907	70700
908	71000
909	71000
910	92300
911	92300
912	91800
913	91800
914	91800
915	91800
916	91410
917	91410
918	91300
919	91300
920	90800
921	90800
922	90800
923	90800
924	90800
925	90800
926	90800
927	90300
928	90300
929	90300
930	90500
931	90500
932	82700
933	82700
934	82900
935	82900
936	82900
937	82900
938	82600
939	82600
940	81800
941	81800
942	80000
943	80000
944	82400
945	82400
946	82300
947	82300
948	83700
949	83700
950	80400
951	80400
952	80800
953	80800
954	83200
955	83200
956	83200
957	83400
958	83400
959	83600
960	83600
961	70100
962	60125
963	81500
964	90410
965	83200
965	83300
966	91400
967	82800
968	70100
969	90700
\.


--
-- Data for Name: g_ct2rls; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct2rls (src, dst) FROM stdin;
885	604
652	1503
263	612
825	1606
497	606
760	1604
838	1606
34	609
522	613
779	1607
146	609
416	602
945	1604
291	613
223	612
15	605
474	604
276	606
689	1501
211	605
231	612
71	603
595	1301
496	606
167	609
365	607
936	1612
72	603
324	603
693	1502
145	609
270	606
754	1604
260	612
64	609
788	1605
163	609
742	1605
473	604
321	603
353	602
687	1501
486	606
957	1606
454	601
613	1402
314	604
930	1502
268	606
774	1607
951	1605
480	604
177	609
768	1604
186	609
579	1301
886	604
590	1301
836	1606
439	601
21	605
475	604
89	603
265	606
5	604
123	607
847	1606
569	1301
39	609
691	1502
654	1503
653	1503
619	1402
325	603
438	601
118	608
575	1301
966	1503
566	1301
717	1502
777	1607
361	607
608	1402
140	607
125	607
178	609
578	1301
556	1301
635	1503
912	1402
340	602
547	1301
57	603
308	611
661	1503
532	1301
540	1301
856	1606
543	1301
611	1402
33	609
446	601
411	611
13	605
160	609
766	1604
375	608
66	609
434	601
794	1605
259	612
656	1503
283	613
618	1402
570	1301
889	602
536	1301
306	611
468	604
897	1301
447	601
484	604
866	1606
215	605
19	605
317	604
229	612
823	1606
843	1606
245	612
751	1605
869	1608
782	1608
873	1612
593	1301
430	601
581	1301
168	609
673	1501
335	603
690	1501
55	607
517	613
645	1402
68	607
128	607
489	606
225	612
597	1402
93	603
457	601
407	611
956	1606
319	604
753	1604
464	601
504	606
598	1402
724	1502
576	1301
721	1502
133	607
530	1301
679	1501
963	1604
873	1402
82	603
757	1604
380	608
809	1605
744	1605
916	1402
115	608
911	1402
149	609
402	611
848	1606
896	604
923	1503
294	613
460	601
602	1402
589	1301
410	611
103	608
851	1608
799	1605
919	1503
441	601
817	1606
720	1502
894	611
65	607
546	1301
366	607
52	607
450	601
816	1606
202	605
391	602
222	612
750	1605
216	605
396	607
861	1606
290	613
827	1606
330	603
944	1604
257	612
958	1606
812	1606
868	1608
874	1402
346	602
692	1501
917	1402
950	1605
105	608
878	1612
565	1301
510	613
348	602
24	605
834	1606
928	1501
839	1606
535	1301
355	602
933	1612
795	1605
887	603
781	1608
151	609
242	612
612	1402
463	601
715	1502
32	605
367	607
190	612
731	1604
616	1402
874	1612
111	608
227	612
651	1503
392	607
247	612
833	1606
615	1402
815	1606
711	1612
214	605
787	1605
726	1502
699	1612
967	1612
681	1501
246	612
506	613
20	605
310	604
777	1608
837	1606
164	609
406	611
520	613
238	607
767	1604
550	1301
577	1301
775	1607
511	613
184	609
235	612
172	609
248	612
158	609
274	606
622	1402
209	605
326	603
939	1603
55	609
746	1605
78	603
136	607
949	1608
170	609
707	1612
85	603
952	1606
797	1605
343	602
451	601
513	613
373	607
38	609
623	1402
877	1612
853	1608
747	1605
876	1612
940	1604
655	1503
359	607
277	606
549	1301
918	1503
137	607
378	608
628	1402
302	611
732	1604
965	1606
740	1605
861	1608
710	1612
224	612
779	1608
27	605
749	1605
762	1604
500	606
633	1503
702	1612
639	1612
119	607
521	613
485	606
883	605
899	1301
516	613
774	1608
808	1605
632	1402
813	1606
943	1605
509	613
798	1605
605	1402
819	1606
196	605
218	605
709	1612
662	1503
555	1301
249	612
541	1301
604	1402
708	1612
96	603
120	607
499	606
25	605
697	1612
784	1605
422	601
792	1605
841	1606
37	609
425	601
52	609
639	1402
929	1502
26	605
213	605
357	602
930	1507
523	1301
868	1607
969	1503
865	1606
703	1612
498	606
716	1502
554	1301
667	1503
31	605
189	609
64	607
479	604
17	605
431	601
423	601
471	604
526	1301
465	604
769	1604
559	1301
537	1301
563	1301
493	606
514	613
382	602
333	603
875	1606
934	1612
110	608
734	1604
191	605
414	602
299	613
91	603
165	609
524	1301
181	609
641	1401
139	607
305	611
11	605
143	607
900	1301
152	609
77	603
947	1604
686	1501
761	1604
278	606
448	601
73	603
764	1604
932	1612
239	607
315	604
852	1608
790	1605
684	1501
735	1604
83	603
921	1503
2	604
872	1608
47	609
436	601
743	1605
621	1402
241	607
286	611
114	608
927	1501
332	603
420	601
672	1501
770	1607
66	607
820	1606
954	1606
142	607
478	604
198	605
818	1606
492	606
262	612
674	1501
698	1612
445	601
594	1301
409	611
664	1503
607	1402
713	1603
882	607
582	1301
863	1606
46	609
193	605
131	607
339	602
117	608
846	1606
153	609
337	603
494	606
466	604
10	605
791	1605
266	606
822	1606
12	605
384	602
56	609
40	609
182	609
588	1301
962	601
453	601
18	605
135	607
850	1608
898	1301
320	603
533	1301
793	1605
665	1503
398	611
737	1604
74	603
842	1606
107	608
519	613
738	1605
59	609
124	607
627	1402
345	602
452	601
904	1301
539	1301
84	603
256	612
42	609
23	605
636	1402
41	603
70	607
455	601
4	604
925	1503
525	1301
680	1501
298	613
938	1603
200	605
678	1501
60	607
811	1606
108	608
244	612
574	1301
831	1606
705	1612
232	612
90	603
867	1608
208	604
237	607
364	607
730	1604
372	607
783	1608
81	603
631	1402
712	1604
79	603
360	607
383	602
926	1503
159	609
583	1301
805	1605
437	601
568	1301
58	607
528	1301
16	605
338	603
467	604
112	608
311	604
53	607
929	1501
469	604
388	602
156	609
796	1605
677	1501
279	613
659	1503
895	604
289	613
859	1608
634	1503
915	1402
258	612
371	607
596	1402
374	608
649	1503
544	1301
828	1606
572	1301
729	1604
76	603
368	607
946	1604
199	605
221	612
334	603
682	1501
403	611
487	606
657	1503
821	1606
562	1301
300	611
54	607
892	611
642	1402
906	1301
50	609
449	601
924	1503
22	605
855	1606
206	604
236	607
358	608
610	1402
254	612
51	609
561	1301
920	1503
727	1507
113	608
905	1301
377	608
138	607
778	1607
696	1507
909	1301
336	603
1	604
100	608
344	602
830	1606
780	1608
935	1612
937	1612
210	605
269	606
573	1301
840	1606
185	609
671	1503
891	611
725	1507
609	1402
728	1604
297	613
668	1503
304	611
625	1402
14	605
901	1301
884	605
890	602
807	1605
240	607
130	607
476	604
862	1606
280	613
97	603
472	604
226	612
666	1503
412	611
415	602
400	611
893	611
600	1402
960	1608
902	1301
233	612
941	1604
63	607
155	609
736	1604
3	604
201	605
801	1605
758	1604
755	1604
220	605
802	1605
442	601
643	1402
913	1402
584	1301
187	609
860	1608
482	604
527	1301
428	601
502	606
212	605
515	613
7	604
95	603
907	1301
179	609
301	611
273	606
592	1301
99	603
845	1606
261	612
94	603
401	611
342	602
637	1402
948	1608
603	1402
424	601
638	1402
955	1606
507	613
759	1604
217	605
942	1605
650	1503
826	1606
347	602
785	1605
329	603
175	609
786	1605
121	607
303	611
271	606
80	603
548	1301
48	609
648	1503
931	1507
281	613
458	601
927	1502
61	607
9	605
701	1612
844	1606
704	1612
835	1606
880	608
287	613
501	606
858	1608
162	609
908	1301
180	609
875	1612
663	1503
293	613
376	608
62	609
879	608
646	1402
404	611
488	606
267	606
538	1301
745	1605
234	612
350	602
551	1301
188	609
903	1301
601	1402
173	609
647	1402
683	1501
765	1604
700	1612
419	601
459	601
88	603
508	613
204	605
150	609
444	601
733	1604
28	605
295	613
426	601
685	1501
630	1402
284	611
776	1607
183	609
63	609
606	1402
328	603
30	605
109	608
313	604
385	602
693	1501
571	1301
599	1402
542	1301
829	1606
870	1608
250	612
288	613
585	1301
778	1608
860	1606
470	604
354	602
243	612
363	607
331	603
620	1402
418	602
696	1502
512	613
318	604
312	604
195	605
387	602
771	1607
531	1301
144	609
275	606
814	1606
8	604
483	604
727	1502
122	607
719	1502
756	1604
62	607
104	608
43	609
560	1301
849	1608
127	607
658	1503
197	605
205	605
660	1503
675	1501
166	609
772	1607
553	1301
362	607
386	602
169	609
255	612
857	1606
910	1402
102	608
706	1612
413	611
505	613
695	1502
282	613
176	609
61	609
545	1301
296	613
351	602
408	611
587	1301
804	1605
309	604
433	601
495	606
723	1502
810	1606
739	1605
803	1605
640	1401
370	607
147	609
676	1501
854	1606
129	607
858	1606
154	609
773	1607
157	609
161	609
252	612
228	612
922	1503
87	603
691	1501
381	608
961	1301
867	1607
881	607
98	608
461	601
35	609
352	602
462	601
953	1606
534	1301
58	609
349	602
203	605
558	1301
141	607
397	611
714	1603
101	608
432	601
75	603
688	1501
477	604
429	601
394	607
964	1501
832	1606
399	611
49	609
806	1605
327	603
230	612
968	1301
567	1301
763	1604
45	609
694	1502
435	601
518	613
389	602
752	1604
60	609
718	1502
316	604
106	608
116	607
670	1503
931	1502
669	1503
741	1605
393	607
417	602
86	603
529	1301
503	606
285	611
134	607
914	1402
194	605
59	607
644	1402
586	1301
341	602
440	601
421	601
748	1605
29	605
307	611
725	1502
171	609
491	606
6	604
69	607
552	1301
591	1301
789	1605
614	1402
722	1502
557	1301
292	613
207	604
253	612
859	1606
395	607
443	601
871	1608
54	609
928	1502
824	1606
427	601
617	1402
490	606
148	609
92	603
174	609
36	609
692	1502
272	606
390	602
405	611
132	607
369	607
456	601
864	1606
629	1402
67	607
356	602
800	1605
580	1301
481	604
888	603
959	1608
564	1301
323	603
219	605
322	603
53	609
192	605
626	1402
251	612
379	608
44	609
624	1402
126	607
264	612
\.


--
-- Data for Name: g_ct2rss; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct2rss (src, dst) FROM stdin;
152	6
475	6
401	6
771	16
281	6
462	6
662	15
5	6
747	16
301	6
567	13
461	6
499	6
181	6
165	6
287	6
21	6
917	14
746	16
303	6
293	6
529	13
365	6
731	16
864	16
797	16
586	13
433	6
376	6
226	6
500	6
534	13
612	14
633	15
776	16
13	6
824	16
879	6
515	6
91	6
233	6
859	16
485	6
616	14
873	16
615	14
558	13
189	6
404	6
507	6
944	16
787	16
880	6
468	6
333	6
968	13
484	6
927	15
885	6
753	16
348	6
784	16
867	16
792	16
878	16
332	6
891	6
963	16
645	14
655	15
56	6
597	14
783	16
40	6
182	6
773	16
355	6
674	15
757	16
933	16
277	6
123	6
261	6
672	15
918	15
125	6
77	6
564	13
808	16
140	6
474	6
580	13
943	16
153	6
686	15
73	6
711	16
211	6
46	6
684	15
83	6
304	6
15	6
361	6
798	16
749	16
916	14
832	16
391	6
444	6
274	6
400	6
893	6
602	14
772	16
473	6
320	6
557	13
426	6
47	6
552	13
591	13
412	6
297	6
598	14
419	6
459	6
314	6
346	6
480	6
873	14
337	6
953	16
740	16
280	6
886	6
585	13
216	6
202	6
608	14
266	6
353	6
665	15
766	16
858	16
912	14
256	6
38	6
128	6
456	6
681	15
300	6
854	16
664	15
170	6
403	6
494	6
810	16
55	6
531	13
68	6
374	6
619	14
244	6
232	6
618	14
903	13
427	6
551	13
158	6
113	6
857	16
172	6
377	6
184	6
24	6
289	6
133	6
279	6
100	6
443	6
892	6
416	6
492	6
164	6
948	16
326	6
78	6
278	6
85	6
921	15
936	16
32	6
421	6
611	14
542	13
571	13
358	6
65	6
587	13
52	6
366	6
317	6
791	16
440	6
754	16
960	16
814	16
37	6
793	16
258	6
108	6
19	6
398	6
215	6
221	6
435	6
889	6
396	6
107	6
860	16
961	13
768	16
493	6
319	6
829	16
96	6
760	16
780	16
560	13
743	16
613	14
429	6
519	6
432	6
254	6
112	6
945	16
392	6
896	6
553	13
969	15
298	6
340	6
667	15
498	6
545	13
367	6
790	16
335	6
621	14
718	15
902	13
656	15
931	15
673	15
690	15
149	6
93	6
934	16
951	16
307	6
382	6
694	15
883	6
414	6
449	6
661	15
844	16
835	16
238	6
27	6
584	13
788	16
742	16
230	6
947	16
703	16
761	16
196	6
218	6
573	13
373	6
966	15
764	16
826	16
849	16
635	15
379	6
405	6
955	16
292	6
735	16
168	6
136	6
679	15
26	6
213	6
607	14
137	6
901	13
845	16
265	6
82	6
25	6
654	15
359	6
653	15
397	6
870	16
548	13
101	6
778	16
20	6
339	6
486	6
330	6
437	6
518	6
692	15
268	6
908	13
214	6
310	6
151	6
734	16
384	6
253	6
399	6
119	6
270	6
794	16
538	13
276	6
527	13
932	16
862	16
907	13
722	15
496	6
455	6
592	13
725	15
209	6
497	6
652	15
452	6
251	6
106	6
769	16
120	6
285	6
698	16
840	16
830	16
887	6
264	6
713	16
928	15
186	6
707	16
234	6
177	6
689	15
809	16
744	16
102	6
315	6
71	6
2	6
72	6
343	6
651	15
568	13
583	13
959	16
458	6
876	16
727	15
198	6
877	16
104	6
528	13
413	6
163	6
855	16
64	6
478	6
696	15
145	6
604	14
167	6
699	16
821	16
967	16
250	6
732	16
641	14
424	6
98	6
193	6
381	6
408	6
639	14
904	13
466	6
539	13
243	6
324	6
828	16
18	6
751	16
632	14
762	16
871	16
139	6
146	6
525	13
919	15
143	6
34	6
687	15
505	6
12	6
296	6
574	13
939	16
282	6
428	6
605	14
10	6
321	6
239	6
288	6
31	6
923	15
709	16
906	13
442	6
57	6
622	14
109	6
723	15
66	6
142	6
479	6
697	16
561	13
160	6
89	6
940	16
708	16
905	13
284	6
504	6
512	6
33	6
471	6
909	13
255	6
241	6
357	6
795	16
465	6
325	6
17	6
811	16
831	16
623	14
178	6
710	16
719	15
950	16
882	6
508	6
544	13
489	6
628	14
131	6
295	6
11	6
767	16
572	13
695	15
135	6
228	6
252	6
799	16
842	16
39	6
750	16
639	16
191	6
562	13
702	16
75	6
603	14
922	15
469	6
637	14
124	6
964	15
510	6
229	6
245	6
563	13
415	6
425	6
638	14
688	15
895	6
724	15
894	6
721	15
422	6
327	6
105	6
822	16
495	6
524	13
846	16
60	6
70	6
863	16
45	6
225	6
199	6
900	13
937	16
86	6
523	13
935	16
49	6
820	16
954	16
22	6
360	6
818	16
206	6
646	14
111	6
237	6
364	6
775	16
372	6
554	13
890	6
290	6
759	16
660	15
559	13
526	13
58	6
1	6
537	13
658	15
23	6
44	6
371	6
625	14
582	13
941	16
53	6
222	6
704	16
275	6
736	16
701	16
92	6
758	16
755	16
804	16
451	6
4	6
407	6
257	6
875	16
200	6
588	13
715	15
294	6
368	6
803	16
739	16
36	6
533	13
609	14
898	13
174	6
115	6
888	6
148	6
236	6
208	6
323	6
913	14
322	6
643	14
54	6
342	6
720	15
728	16
103	6
517	6
410	6
242	6
138	6
190	6
600	14
16	6
402	6
467	6
171	6
227	6
865	16
247	6
380	6
311	6
347	6
594	13
685	15
962	6
263	6
841	16
144	6
705	16
7	6
453	6
693	15
938	16
383	6
212	6
328	6
781	16
130	6
272	6
240	6
223	6
819	16
283	6
217	6
231	6
915	14
549	13
813	16
741	16
683	15
596	14
930	15
88	6
118	6
331	6
851	16
63	6
9	6
183	6
260	6
445	6
411	6
806	16
345	6
436	6
150	6
308	6
420	6
490	6
642	14
491	6
729	16
550	13
946	16
306	6
577	13
173	6
965	16
868	16
188	6
375	6
610	14
448	6
35	6
899	13
636	14
800	16
730	16
503	6
210	6
161	6
157	6
712	16
154	6
555	13
541	13
147	6
952	16
121	6
884	6
14	6
670	15
344	6
675	15
476	6
669	15
472	6
627	14
201	6
691	15
61	6
176	6
770	16
737	16
87	6
169	6
388	6
869	16
782	16
522	6
291	6
423	6
3	6
431	6
259	6
837	16
166	6
748	16
789	16
43	6
676	15
717	15
220	6
482	6
631	14
576	13
833	16
920	15
815	16
774	16
205	6
197	6
447	6
530	13
187	6
805	16
114	6
617	14
389	6
97	6
714	16
409	6
446	6
155	6
417	6
434	6
834	16
839	16
341	6
589	13
614	14
286	6
924	15
716	15
309	6
958	16
812	16
349	6
363	6
624	14
626	14
861	16
487	6
827	16
657	15
593	13
581	13
779	16
117	6
816	16
438	6
185	6
817	16
649	15
439	6
659	15
738	16
629	14
634	15
127	6
390	6
763	16
926	15
565	13
949	16
122	6
535	13
752	16
848	16
62	6
514	6
329	6
454	6
180	6
853	16
162	6
30	6
356	6
299	6
95	6
99	6
48	6
28	6
94	6
262	6
313	6
204	6
362	6
925	15
956	16
110	6
370	6
175	6
914	14
470	6
546	13
777	16
644	14
305	6
796	16
80	6
881	6
318	6
312	6
179	6
195	6
129	6
823	16
843	16
640	14
8	6
483	6
41	6
802	16
516	6
866	16
680	15
141	6
663	15
6	6
509	6
463	6
910	14
246	6
29	6
488	6
159	6
267	6
678	15
354	6
648	15
207	6
74	6
418	6
856	16
235	6
801	16
394	6
590	13
387	6
850	16
248	6
579	13
84	6
501	6
807	16
756	16
116	6
350	6
338	6
852	16
700	16
271	6
726	15
650	15
872	16
42	6
502	6
450	6
90	6
481	6
134	6
59	6
192	6
385	6
441	6
595	13
847	16
393	6
81	6
219	6
273	6
521	6
836	16
79	6
532	13
76	6
406	6
682	15
69	6
460	6
543	13
334	6
630	14
540	13
203	6
513	6
745	16
606	14
957	16
599	14
51	6
677	15
224	6
477	6
536	13
666	15
570	13
50	6
929	15
601	14
647	14
897	13
464	6
352	6
395	6
369	6
620	14
569	13
668	15
67	6
457	6
506	6
786	16
316	6
336	6
575	13
132	6
520	6
942	16
765	16
785	16
378	6
566	13
733	16
302	6
386	6
249	6
269	6
126	6
194	6
511	6
430	6
838	16
556	13
351	6
578	13
671	15
706	16
156	6
547	13
825	16
874	14
\.


--
-- Data for Name: g_ct2statoid; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_ct2statoid (src, dst) FROM stdin;
526	10
527	10
528	10
529	10
530	10
531	10
532	10
533	10
552	10
578	10
585	10
589	10
594	10
603	10
616	10
618	10
628	10
648	10
668	10
669	10
671	10
693	10
696	10
699	10
709	10
710	10
714	10
732	10
751	10
755	10
765	10
783	10
796	10
821	10
845	10
857	10
869	10
291	10
292	10
293	10
294	10
295	10
296	10
297	10
298	10
299	10
300	10
301	10
302	10
303	10
304	10
305	10
306	10
307	10
308	10
309	10
310	10
311	10
312	10
313	10
314	10
315	10
316	10
317	10
318	10
320	10
321	10
322	10
323	10
324	10
325	10
326	10
327	10
328	10
329	10
330	10
331	10
332	10
333	10
334	10
335	10
336	10
338	10
339	10
340	10
341	10
342	10
343	10
344	10
345	10
346	10
347	10
348	10
349	10
351	10
352	10
353	10
354	10
355	10
356	10
357	10
358	10
359	10
360	10
361	10
362	10
363	10
364	10
365	10
366	10
367	10
368	10
369	10
370	10
371	10
372	10
373	10
374	10
375	10
376	10
377	10
378	10
379	10
380	10
381	10
382	10
383	10
384	10
385	10
386	10
387	10
388	10
389	10
390	10
391	10
392	10
393	10
394	10
395	10
396	10
397	10
398	10
399	10
400	10
403	10
404	10
405	10
406	10
407	10
408	10
409	10
410	10
411	10
412	10
413	10
414	10
415	10
416	10
417	10
418	10
419	10
420	10
421	10
422	10
423	10
424	10
425	10
426	10
427	10
428	10
429	10
430	10
431	10
432	10
433	10
434	10
435	10
438	10
439	10
440	10
441	10
442	10
443	10
444	10
445	10
446	10
447	10
448	10
449	10
450	10
451	10
452	10
453	10
454	10
455	10
456	10
457	10
458	10
459	10
460	10
461	10
462	10
463	10
464	10
465	10
466	10
467	10
468	10
469	10
470	10
471	10
472	10
473	10
474	10
475	10
476	10
477	10
478	10
479	10
480	10
481	10
483	10
484	10
485	10
486	10
487	10
488	10
489	10
490	10
491	10
492	10
493	10
494	10
534	10
535	10
536	10
537	10
538	10
539	10
540	10
541	10
542	10
543	10
544	10
545	10
546	10
547	10
548	10
549	10
550	10
551	10
553	10
554	10
555	10
556	10
557	10
558	10
559	10
560	10
561	10
562	10
563	10
564	10
565	10
566	10
567	10
568	10
573	10
574	10
575	10
576	10
577	10
579	10
580	10
581	10
582	10
583	10
584	10
586	10
587	10
588	10
590	10
591	10
592	10
593	10
595	10
596	10
597	10
1	10
2	10
3	10
4	10
5	10
6	10
7	10
8	10
9	10
10	10
11	10
12	10
13	10
14	10
15	10
16	10
17	10
18	10
19	10
20	10
21	10
22	10
23	10
24	10
25	10
26	10
27	10
28	10
29	10
30	10
31	10
32	10
33	10
34	10
35	10
36	10
37	10
38	10
39	10
598	10
599	10
600	10
601	10
602	10
604	10
605	10
606	10
607	10
608	10
609	10
610	10
611	10
612	10
613	10
614	10
615	10
617	10
619	10
620	10
621	10
622	10
623	10
624	10
625	10
626	10
627	10
629	10
630	10
631	10
632	10
633	10
636	10
637	10
638	10
639	10
640	10
641	10
642	10
643	10
644	10
645	10
646	10
647	10
649	10
650	10
651	10
652	10
653	10
654	10
655	10
656	10
657	10
660	10
661	10
662	10
663	10
664	10
665	10
666	10
667	10
670	10
672	10
673	10
674	10
677	10
678	10
679	10
680	10
681	10
682	10
683	10
684	10
685	10
686	10
687	10
688	10
689	10
690	10
691	10
692	10
694	10
695	10
697	10
698	10
700	10
701	10
704	10
705	10
706	10
707	10
708	10
711	10
712	10
713	10
715	10
716	10
717	10
718	10
719	10
720	10
721	10
722	10
723	10
724	10
725	10
726	10
727	10
728	10
729	10
730	10
731	10
733	10
734	10
735	10
736	10
737	10
738	10
739	10
740	10
741	10
742	10
743	10
744	10
745	10
746	10
747	10
748	10
749	10
750	10
752	10
753	10
754	10
756	10
757	10
758	10
759	10
762	10
763	10
764	10
766	10
767	10
768	10
769	10
770	10
771	10
772	10
773	10
774	10
775	10
776	10
777	10
778	10
779	10
780	10
781	10
782	10
784	10
785	10
786	10
787	10
788	10
789	10
790	10
791	10
792	10
793	10
794	10
795	10
797	10
798	10
799	10
800	10
801	10
802	10
803	10
804	10
805	10
806	10
807	10
808	10
40	10
41	10
42	10
43	10
44	10
45	10
46	10
47	10
48	10
49	10
50	10
51	10
52	10
53	10
54	10
55	10
56	10
57	10
58	10
59	10
60	10
61	10
62	10
63	10
64	10
65	10
66	10
67	10
68	10
69	10
70	10
71	10
72	10
73	10
74	10
75	10
76	10
77	10
78	10
79	10
80	10
81	10
82	10
83	10
84	10
85	10
86	10
87	10
88	10
89	10
90	10
91	10
92	10
93	10
94	10
95	10
96	10
97	10
98	10
99	10
100	10
101	10
102	10
103	10
104	10
105	10
107	10
108	10
109	10
110	10
111	10
112	10
113	10
114	10
115	10
116	10
117	10
118	10
106	10
809	10
810	10
811	10
812	10
813	10
814	10
815	10
816	10
817	10
818	10
819	10
820	10
822	10
823	10
824	10
825	10
826	10
827	10
828	10
829	10
830	10
831	10
832	10
833	10
834	10
835	10
836	10
837	10
838	10
839	10
840	10
841	10
846	10
847	10
848	10
849	10
850	10
851	10
852	10
853	10
854	10
855	10
856	10
858	10
859	10
860	10
861	10
862	10
863	10
864	10
865	10
866	10
867	10
868	10
870	10
871	10
872	10
204	10
205	10
206	10
207	10
208	10
209	10
210	10
119	10
120	10
121	10
122	10
123	10
124	10
125	10
126	10
127	10
128	10
130	10
131	10
132	10
133	10
134	10
135	10
136	10
137	10
138	10
139	10
140	10
141	10
142	10
143	10
144	10
145	10
146	10
147	10
148	10
149	10
150	10
151	10
152	10
153	10
154	10
155	10
156	10
157	10
158	10
159	10
160	10
161	10
162	10
163	10
164	10
165	10
166	10
167	10
168	10
169	10
170	10
171	10
172	10
173	10
174	10
175	10
176	10
177	10
211	10
212	10
213	10
214	10
215	10
216	10
217	10
218	10
219	10
220	10
221	10
222	10
223	10
224	10
225	10
226	10
227	10
228	10
229	10
230	10
231	10
232	10
233	10
234	10
235	10
236	10
237	10
238	10
239	10
240	10
241	10
242	10
243	10
244	10
245	10
246	10
247	10
248	10
249	10
250	10
251	10
252	10
253	10
254	10
255	10
256	10
257	10
258	10
259	10
260	10
261	10
178	10
179	10
180	10
181	10
182	10
183	10
184	10
185	10
186	10
187	10
188	10
189	10
190	10
191	10
192	10
193	10
194	10
195	10
196	10
197	10
198	10
200	10
201	10
202	10
203	10
129	10
199	10
570	10
436	10
437	10
569	10
571	10
572	10
634	10
635	10
658	10
659	10
675	10
676	10
702	10
703	10
760	10
761	10
879	10
880	10
881	10
882	10
883	10
884	10
885	10
886	10
887	10
888	10
889	10
890	10
891	10
892	10
893	10
894	10
262	10
263	10
264	10
265	10
266	10
267	10
268	10
269	10
270	10
271	10
272	10
273	10
274	10
275	10
276	10
277	10
278	10
279	10
280	10
281	10
282	10
283	10
284	10
285	10
286	10
287	10
288	10
289	10
290	10
319	10
337	10
350	10
401	10
402	10
482	10
495	10
496	10
497	10
498	10
499	10
500	10
501	10
502	10
503	10
504	10
505	10
506	10
507	10
508	10
509	10
510	10
511	10
512	10
513	10
514	10
515	10
516	10
517	10
518	10
519	10
520	10
521	10
522	10
523	10
524	10
525	10
895	10
896	10
897	10
898	10
899	10
900	10
901	10
902	10
903	10
904	10
905	10
906	10
907	10
908	10
909	10
910	10
911	10
912	10
913	10
914	10
915	10
916	10
917	10
918	10
919	10
920	10
921	10
922	10
923	10
924	10
925	10
926	10
927	10
928	10
929	10
930	10
931	10
932	10
933	10
934	10
935	10
936	10
937	10
938	10
939	10
940	10
941	10
942	10
943	10
944	10
945	10
946	10
947	10
948	10
949	10
950	10
951	10
842	10
843	10
844	10
873	10
874	10
875	10
876	10
877	10
878	10
961	10
962	10
963	10
964	10
965	10
966	10
967	10
968	10
969	10
952	10
953	10
954	10
955	10
956	10
957	10
958	10
959	10
960	10
\.


--
-- Data for Name: g_neigh; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_neigh (id, shortform) FROM stdin;
81800	phio:neight_81800
80200	phio:neight_80200
60223	phio:neight_60223
80600	phio:neight_80600
60115	phio:neight_60115
60431	phio:neight_60431
60121	phio:neight_60121
99999	phio:neight_99999
60932	phio:neight_60932
61112	phio:neight_61112
60824	phio:neight_60824
82600	phio:neight_82600
60812	phio:neight_60812
60523	phio:neight_60523
90500	phio:neight_90500
70500	phio:neight_70500
83200	phio:neight_83200
83100	phio:neight_83100
60712	phio:neight_60712
90900	phio:neight_90900
61323	phio:neight_61323
91700	phio:neight_91700
84000	phio:neight_84000
83600	phio:neight_83600
60226	phio:neight_60226
61312	phio:neight_61312
90700	phio:neight_90700
60622	phio:neight_60622
61113	phio:neight_61113
91500	phio:neight_91500
61123	phio:neight_61123
60423	phio:neight_60423
90100	phio:neight_90100
83000	phio:neight_83000
60433	phio:neight_60433
80100	phio:neight_80100
82400	phio:neight_82400
61324	phio:neight_61324
60814	phio:neight_60814
60123	phio:neight_60123
82000	phio:neight_82000
61212	phio:neight_61212
60711	phio:neight_60711
70200	phio:neight_70200
81300	phio:neight_81300
60923	phio:neight_60923
81500	phio:neight_81500
61313	phio:neight_61313
61221	phio:neight_61221
60324	phio:neight_60324
70800	phio:neight_70800
60743	phio:neight_60743
72000	phio:neight_72000
60921	phio:neight_60921
60127	phio:neight_60127
70700	phio:neight_70700
92300	phio:neight_92300
91800	phio:neight_91800
60126	phio:neight_60126
60751	phio:neight_60751
70300	phio:neight_70300
60621	phio:neight_60621
60436	phio:neight_60436
60343	phio:neight_60343
60823	phio:neight_60823
61124	phio:neight_61124
92200	phio:neight_92200
80300	phio:neight_80300
90600	phio:neight_90600
70000	phio:neight_70000
82100	phio:neight_82100
91200	phio:neight_91200
71000	phio:neight_71000
83400	phio:neight_83400
60128	phio:neight_60128
81000	phio:neight_81000
60421	phio:neight_60421
82500	phio:neight_82500
60822	phio:neight_60822
60129	phio:neight_60129
60213	phio:neight_60213
60931	phio:neight_60931
60222	phio:neight_60222
60612	phio:neight_60612
60215	phio:neight_60215
90410	phio:neight_90410
60211	phio:neight_60211
60821	phio:neight_60821
60713	phio:neight_60713
60322	phio:neight_60322
60623	phio:neight_60623
71100	phio:neight_71100
90400	phio:neight_90400
60522	phio:neight_60522
60225	phio:neight_60225
60221	phio:neight_60221
61321	phio:neight_61321
60323	phio:neight_60323
60521	phio:neight_60521
71500	phio:neight_71500
92100	phio:neight_92100
91300	phio:neight_91300
80000	phio:neight_80000
60513	phio:neight_60513
60922	phio:neight_60922
60432	phio:neight_60432
82900	phio:neight_82900
91100	phio:neight_91100
92110	phio:neight_92110
61111	phio:neight_61111
60531	phio:neight_60531
60311	phio:neight_60311
60122	phio:neight_60122
60437	phio:neight_60437
70600	phio:neight_70600
83800	phio:neight_83800
60111	phio:neight_60111
92000	phio:neight_92000
60112	phio:neight_60112
60411	phio:neight_60411
60114	phio:neight_60114
60535	phio:neight_60535
81400	phio:neight_81400
80400	phio:neight_80400
61325	phio:neight_61325
61121	phio:neight_61121
82300	phio:neight_82300
83110	phio:neight_83110
61211	phio:neight_61211
91000	phio:neight_91000
91900	phio:neight_91900
91410	phio:neight_91410
90200	phio:neight_90200
80800	phio:neight_80800
60212	phio:neight_60212
83300	phio:neight_83300
82200	phio:neight_82200
60825	phio:neight_60825
61213	phio:neight_61213
60435	phio:neight_60435
82700	phio:neight_82700
60811	phio:neight_60811
60742	phio:neight_60742
70100	phio:neight_70100
60214	phio:neight_60214
91600	phio:neight_91600
80500	phio:neight_80500
60511	phio:neight_60511
70400	phio:neight_70400
90300	phio:neight_90300
70900	phio:neight_70900
80710	phio:neight_80710
60924	phio:neight_60924
81900	phio:neight_81900
80700	phio:neight_80700
60911	phio:neight_60911
81100	phio:neight_81100
60422	phio:neight_60422
60124	phio:neight_60124
83700	phio:neight_83700
80900	phio:neight_80900
60815	phio:neight_60815
60533	phio:neight_60533
82800	phio:neight_82800
60125	phio:neight_60125
83900	phio:neight_83900
83500	phio:neight_83500
61122	phio:neight_61122
91400	phio:neight_91400
60321	phio:neight_60321
61125	phio:neight_61125
90000	phio:neight_90000
71300	phio:neight_71300
60113	phio:neight_60113
60714	phio:neight_60714
71400	phio:neight_71400
60325	phio:neight_60325
61222	phio:neight_61222
60813	phio:neight_60813
60434	phio:neight_60434
60341	phio:neight_60341
60741	phio:neight_60741
60412	phio:neight_60412
60611	phio:neight_60611
60512	phio:neight_60512
60532	phio:neight_60532
61311	phio:neight_61311
90800	phio:neight_90800
81200	phio:neight_81200
61322	phio:neight_61322
60116	phio:neight_60116
60534	phio:neight_60534
60224	phio:neight_60224
60912	phio:neight_60912
60342	phio:neight_60342
60613	phio:neight_60613
\.


--
-- Data for Name: g_rls; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_rls (id, shortform) FROM stdin;
104	geo:rls_de_La_Matapedia
804	geo:rls_de_l_Abitibi-Ouest
502	geo:rls_d_Asbestos
1607	geo:rls_du_Haut-Richelieu_-_Rouville
506	geo:rls_de_Memphremagog
601	geo:rls_de_Pierrefonds_-_Lac_Saint-Louis
1102	geo:rls_du_Rocher-Perce
1502	geo:rls_de_la_Riviere-du-Nord_-_Mirabel-Nord
1501	geo:rls_de_Deux-Montagnes_-_Mirabel-Sud
405	geo:rls_de_Trois-Rivieres
805	geo:rls_de_l_Abitibi
1506	geo:rls_des_Laurentides
301	geo:rls_de_Portneuf
1611	geo:rls_de_la_Haute-Yamaska
402	geo:rls_de_la_Vallee_de_la_Batiscan
503	geo:rls_du_Haut-Saint-Francois
504	geo:rls_de_Val_Saint-Francois
1610	geo:rls_de_la_Pommeraie
807	geo:rls_du_Temiscaming
1203	geo:rls_de_Beauce
507	geo:rls_de_Sherbrooke
1603	geo:rls_du_Suroit
1101	geo:rls_de_la_Baie-des-Chaleurs
1103	geo:rls_de_La_Cote-de-Gaspe
701	geo:rls_de_Grande-Riviere_-_Hull_-_Gatineau
1608	geo:rls_de_Richelieu-Yamaska
101	geo:rls_de_Rimouski
1204	geo:rls_de_la_region_de_Thetford
501	geo:rls_du_Granit
1606	geo:rls_Pierre-Boucher
604	geo:rls_de_Riviere-des-Prairies_-_Anjou_-_Montreal-Est
902	geo:rls_de_Manicouagan
304	geo:rls_de_Charlevoix
903	geo:rls_de_Port-Cartier
407	geo:rls_de_Drummond
803	geo:rls_de_Rouyn-Noranda
1402	geo:rls_de_Lanaudiere-Sud
901	geo:rls_de_la_Haute-Cote-Nord
201	geo:rls_de_La_Baie
806	geo:rls_de_la_Vallee-de-l_Or
202	geo:rls_de_Chicoutimi
608	geo:rls_de_Cote-Saint-Luc_-_NDG_-_Montreal-Ouest
205	geo:rls_de_Maria-Chapdelaine
106	geo:rls_de_Temiscouata
908	geo:rls_de_Kawawachikamach
102	geo:rls_de_La_Mitis
1202	geo:rls_Alphonse-Desjardins
603	geo:rls_de_Verdun_-_Cote_St-Paul_-_St-Henri_-_Pointe-St-Charles
702	geo:rls_du_Pontiac
907	geo:rls_de_la_Basse-Cote-Nord
1602	geo:rls_du_Haut-Saint-Laurent
1612	geo:rls_de_Vaudreuil-Soulanges
203	geo:rls_de_Jonquiere
406	geo:rls_de_Becancour_-_Nicolet-Yamaska
1201	geo:rls_des_Etchemins
611	geo:rls_du_Nord_de_l_Ile_-_Saint-Laurent
1605	geo:rls_de_Champlain
704	geo:rls_de_la_Vallee-de-la-Gatineau
1504	geo:rls_d_Antoine-Labelle
107	geo:rls_de_Riviere-du-Loup
403	geo:rls_du_Centre-de-la-Mauricie
1609	geo:rls_Pierre-De_Saurel
1205	geo:rls_de_Montmagny-L_Islet
612	geo:rls_de_la_Petite_Patrie_-_Villeray
105	geo:rls_des_Basques
401	geo:rls_du_Haut-Saint-Maurice
613	geo:rls_d_Ahuntsic_-_Montreal-Nord
904	geo:rls_de_Sept-Iles
1401	geo:rls_de_Lanaudiere-Nord
505	geo:rls_de_Coaticook
108	geo:rls_de_Kamouraska
1503	geo:rls_de_Therese-De_Blainville
408	geo:rls_d_Arthabaska_-_de_l_Erable
705	geo:rls_de_la_Vallee-de-la-Lievre_et_de_la_Petite-Nation
1507	geo:rls_d_Argenteuil
606	geo:rls_de_Saint-Leonard_-_Saint-Michel
703	geo:rls_des_Collines-de-l_Outaouais
605	geo:rls_de_Hochelaga_-_Mercier-Ouest_-_Rosemont
206	geo:rls_de_Lac-Saint-Jean-Est
404	geo:rls_de_Maskinonge
906	geo:rls_de_la_Minganie
1301	geo:rls_de_Laval
303	geo:rls_de_Quebec-Nord
1604	geo:rls_de_Jardins-Roussillon
302	geo:rls_de_Quebec-Sud
607	geo:rls_de_Cote-des-Neiges_-_Metro_-_Parc-Extension
1105	geo:rls_de_la_Haute-Gaspesie
103	geo:rls_de_Matane
905	geo:rls_de_Caniapiscau
602	geo:rls_de_Dorval_-_Lachine_-_Lasalle
609	geo:rls_des_Faubourgs_-_Plateau-Mont-Royal_-_St-Louis-du-Parc
1104	geo:rls_des_Iles-de-la-Madeleine
1505	geo:rls_des_Pays-d_en-Haut
204	geo:rls_du_Domaine-du-Roy
1710	geo:rls_rss_Nunavik
1010	geo:rls_rss_Nord-du-Quebec
1810	geo:rls_rss_Terres-Cries-de-la-Baie-James
\.


--
-- Data for Name: g_rls2rss; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_rls2rss (src, dst) FROM stdin;
105	1
108	1
102	1
106	1
104	1
103	1
101	1
107	1
205	2
201	2
203	2
206	2
204	2
202	2
301	3
302	3
303	3
304	3
401	4
404	4
402	4
408	4
407	4
403	4
406	4
405	4
503	5
502	5
504	5
505	5
506	5
501	5
507	5
601	6
602	6
603	6
608	6
607	6
611	6
613	6
612	6
609	6
606	6
605	6
604	6
702	7
704	7
703	7
701	7
705	7
804	8
805	8
803	8
806	8
807	8
906	9
903	9
908	9
904	9
907	9
905	9
901	9
902	9
1105	11
1102	11
1104	11
1101	11
1103	11
1201	12
1204	12
1205	12
1203	12
1202	12
1301	13
1402	14
1401	14
1505	15
1503	15
1506	15
1504	15
1501	15
1502	15
1507	15
1606	16
1607	16
1609	16
1603	16
1604	16
1610	16
1608	16
1612	16
1605	16
1602	16
1611	16
1010	10
1710	17
1810	18
\.


--
-- Data for Name: g_rls2statoid; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_rls2statoid (src, dst) FROM stdin;
104	10
804	10
502	10
1607	10
506	10
601	10
1102	10
1502	10
1501	10
405	10
805	10
1506	10
301	10
1611	10
402	10
503	10
504	10
1610	10
807	10
1203	10
507	10
1603	10
1101	10
1103	10
701	10
1608	10
101	10
1204	10
501	10
1606	10
604	10
902	10
304	10
903	10
407	10
803	10
1402	10
901	10
201	10
806	10
202	10
608	10
205	10
106	10
908	10
102	10
1202	10
603	10
702	10
907	10
1602	10
1612	10
203	10
406	10
1201	10
611	10
1605	10
704	10
1504	10
107	10
403	10
1609	10
1205	10
612	10
105	10
401	10
613	10
904	10
1401	10
505	10
108	10
1503	10
408	10
705	10
1507	10
606	10
703	10
605	10
206	10
404	10
906	10
1301	10
303	10
1604	10
302	10
607	10
1105	10
103	10
905	10
602	10
609	10
1104	10
1505	10
204	10
1710	10
1010	10
1810	10
\.


--
-- Data for Name: g_rss; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_rss (id, shortform) FROM stdin;
9	geo:rss_Cote-Nord
4	geo:rss_Mauricie_et_Centre-du-Quebec
10	geo:rss_Nord-du-Quebec
14	geo:rss_Lanaudiere
8	geo:rss_Abitibi-Temiscamingue
11	geo:rss_Gaspesie_-_Iles-de-la-Madeleine
16	geo:rss_Monteregie
15	geo:rss_Laurentides
17	geo:rss_Nunavik
3	geo:rss_Capitale-Nationale
12	geo:rss_Chaudiere-Appalaches
18	geo:rss_Terres-Cries-de-la-Baie-James
2	geo:rss_Saguenay_-_Lac-Saint-Jean
6	geo:rss_Montreal
13	geo:rss_Laval
1	geo:rss_Bas-Saint-Laurent
7	geo:rss_Outaouais
5	geo:rss_Estrie
\.


--
-- Data for Name: g_rss2statoid; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_rss2statoid (src, dst) FROM stdin;
9	10
4	10
10	10
14	10
8	10
11	10
16	10
15	10
17	10
3	10
12	10
18	10
2	10
6	10
13	10
1	10
7	10
5	10
\.


--
-- Data for Name: g_statoid; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.g_statoid (id, shortform, abbrev, en, fr) FROM stdin;
6	geo:Nova_Scotia	NS	Nova Scotia              	Nouvelle-Écosse          
4	geo:Newfoundland_and_Labrador	NL	Newfoundland and Labrador	Terre-Neuve-et-Labrador  
7	geo:Nunavut	NU	Nunavut                  	Nunavut                  
8	geo:Ontario	ON	Ontario                  	Ontario                  
3	geo:New_Brunswick	NB	New Brunswick            	Nouveau-Brunswick        
10	geo:Quebec	QC	Quebec                   	Québec                   
1	geo:British_Columbia	BC	British Columbia         	Colombie-Britannique     
9	geo:Prince_Edward_Island	PE	Prince Edward Island     	Île-du-Prince-Édouard    
11	geo:Saskatchewan	SK	Saskatchewan             	Saskatchewan             
13	geo:Alberta	AB	Alberta                  	Alberta                  
2	geo:Manitoba	MB	Manitoba                 	Manitoba                 
12	geo:Yukon	YT	Yukon                    	Yukon                    
5	geo:Northwest_Territories	NT	Northwest Territories    	Territoires du Nord-Ouest
\.


--
-- Data for Name: mappings; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.mappings (type, src, dst, version, tablename) FROM stdin;
GEO	phio:stratification_CT	phio:stratification_neighborhood	0	g_ct2neigh
GEO	phio:stratification_CT	phio:stratification_CLSC	0	g_ct2clsc
GEO	phio:stratification_CT	phio:stratification_RSS	0	g_ct2rss
AGE	phio:stratification_1yr_age_groups	phio:stratification_10yr_age_groups	0	a_1to10yr
AGE	phio:stratification_1yr_age_groups	phio:stratification_5yr_age_groups	0	a_1to5yr
AGE	phio:stratification_1yr_age_groups	phio:stratification_children_adults_elderly	0	a_1tocae
GEO	phio:stratification_CT	phio:stratification_RLS	0	g_ct2rls
GEO	phio:stratification_RLS	phio:stratification_RSS	0	g_rls2rss
GEO	phio:stratification_CLSC	phio:stratification_RLS	0	g_clsc2rls
GEO	phio:stratification_CLSC	phio:stratification_RSS	0	g_clsc2rss
GEO	phio:stratification_CT	phio:ProvinceOrTerritory	0	g_ct2statoid
GEO	phio:stratification_CLSC	phio:ProvinceOrTerritory	0	g_clsc2statoid
GEO	phio:stratification_RLS	phio:ProvinceOrTerritory	0	g_rls2statoid
GEO	phio:stratification_RSS	phio:ProvinceOrTerritory	0	r_rss2statoid
\.


--
-- Data for Name: sex; Type: TABLE DATA; Schema: dim; Owner: -
--

COPY dim.sex (id, shortform) FROM stdin;
0	phio:female
1	phio:male
\.


--
-- Name: a_1tocae a_1tocae_pkey; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_1tocae
    ADD CONSTRAINT a_1tocae_pkey PRIMARY KEY (src, dst);


--
-- Name: a_cae a_cae_pkey; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_cae
    ADD CONSTRAINT a_cae_pkey PRIMARY KEY (id);


--
-- Name: g_ct g_ct_pkey; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct
    ADD CONSTRAINT g_ct_pkey PRIMARY KEY (id);


--
-- Name: a_10yr pk_10yr; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_10yr
    ADD CONSTRAINT pk_10yr PRIMARY KEY (id);


--
-- Name: a_1to10yr pk_1to10; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_1to10yr
    ADD CONSTRAINT pk_1to10 PRIMARY KEY (src, dst);


--
-- Name: a_1to5yr pk_ag_1yr_to_5yr; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_1to5yr
    ADD CONSTRAINT pk_ag_1yr_to_5yr PRIMARY KEY (src, dst);


--
-- Name: a_5yr pk_age_5yr; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.a_5yr
    ADD CONSTRAINT pk_age_5yr PRIMARY KEY (id);


--
-- Name: g_clsc2rls pk_clsc2rls; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_clsc2rls
    ADD CONSTRAINT pk_clsc2rls PRIMARY KEY (src, dst);


--
-- Name: g_clsc2rss pk_clsc2rss; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_clsc2rss
    ADD CONSTRAINT pk_clsc2rss PRIMARY KEY (src, dst);


--
-- Name: g_clsc2statoid pk_clsc2statoid; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_clsc2statoid
    ADD CONSTRAINT pk_clsc2statoid PRIMARY KEY (src, dst);


--
-- Name: codelists pk_codelists; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.codelists
    ADD CONSTRAINT pk_codelists PRIMARY KEY (shortform);


--
-- Name: g_ct2clsc pk_ct2clsc; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct2clsc
    ADD CONSTRAINT pk_ct2clsc PRIMARY KEY (src, dst);


--
-- Name: g_ct2rls pk_ct2rls; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct2rls
    ADD CONSTRAINT pk_ct2rls PRIMARY KEY (src, dst);


--
-- Name: g_ct2rss pk_ct2rss; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct2rss
    ADD CONSTRAINT pk_ct2rss PRIMARY KEY (src, dst);


--
-- Name: g_ct2statoid pk_ct2statoid; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct2statoid
    ADD CONSTRAINT pk_ct2statoid PRIMARY KEY (src, dst);


--
-- Name: g_ct2neigh pk_geo_ct_to_neigh; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_ct2neigh
    ADD CONSTRAINT pk_geo_ct_to_neigh PRIMARY KEY (src, dst);


--
-- Name: g_neigh pk_geo_neigh; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_neigh
    ADD CONSTRAINT pk_geo_neigh PRIMARY KEY (id);


--
-- Name: mappings pk_mappings; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.mappings
    ADD CONSTRAINT pk_mappings PRIMARY KEY (src, dst, version);


--
-- Name: g_rls pk_rls; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_rls
    ADD CONSTRAINT pk_rls PRIMARY KEY (id);


--
-- Name: g_rls2rss pk_rls2rss; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_rls2rss
    ADD CONSTRAINT pk_rls2rss PRIMARY KEY (src, dst);


--
-- Name: g_rls2statoid pk_rls2statoid; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_rls2statoid
    ADD CONSTRAINT pk_rls2statoid PRIMARY KEY (src, dst);


--
-- Name: g_rss pk_rss; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_rss
    ADD CONSTRAINT pk_rss PRIMARY KEY (id);


--
-- Name: g_rss2statoid pk_rss2statoid; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_rss2statoid
    ADD CONSTRAINT pk_rss2statoid PRIMARY KEY (src, dst);


--
-- Name: sex pk_sex; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.sex
    ADD CONSTRAINT pk_sex PRIMARY KEY (id);


--
-- Name: g_statoid pk_statoid; Type: CONSTRAINT; Schema: dim; Owner: -
--

ALTER TABLE ONLY dim.g_statoid
    ADD CONSTRAINT pk_statoid PRIMARY KEY (id);


--
-- Name: g_ct_name_uindex; Type: INDEX; Schema: dim; Owner: -
--

CREATE UNIQUE INDEX g_ct_name_uindex ON dim.g_ct USING btree (official);


--
-- Name: idx_1to10; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_1to10 ON dim.a_1to10yr USING btree (src);


--
-- Name: idx_clsc2statoid; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_clsc2statoid ON dim.g_clsc2statoid USING btree (src);


--
-- Name: idx_ct2clsc; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_ct2clsc ON dim.g_ct2clsc USING btree (src);


--
-- Name: idx_ct2rls; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_ct2rls ON dim.g_ct2rls USING btree (src);


--
-- Name: idx_ct2rss; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_ct2rss ON dim.g_ct2rss USING btree (src);


--
-- Name: idx_ct2statoid; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_ct2statoid ON dim.g_ct2statoid USING btree (src);


--
-- Name: idx_rls2statoid; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_rls2statoid ON dim.g_rls2statoid USING btree (src);


--
-- Name: idx_rss2statoid; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_rss2statoid ON dim.g_rss2statoid USING btree (src);


--
-- Name: idx_src_ct; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_src_ct ON dim.g_ct2neigh USING btree (src);


--
-- Name: idx_src_year; Type: INDEX; Schema: dim; Owner: -
--

CREATE INDEX idx_src_year ON dim.a_1to5yr USING btree (src);


--
-- PostgreSQL database dump complete
--

