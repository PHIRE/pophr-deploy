#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname pophr <<-EOSQL

-- Creates a base PopHR account needed to then allow more people in­.
INSERT INTO usage."user" (roleid, name, email, uid, createdon, lastseen, lastmodificationtime, experiment, groupname, studypopulation)
  VALUES (6, 'Default PopHR Account', 'pophr.mcgill@gmail.com', '112004627806625110897', '2017-03-09', '2017-03-09', '2018-06-05', 1, '', null);

-- Grants usage on all schemas
GRANT ALL   ON SCHEMA cubes TO payload, pophr, read_only;
GRANT ALL   ON SCHEMA data TO payload, pophr, read_only;
GRANT USAGE ON SCHEMA dim TO payload, pophr, read_only;
GRANT ALL   ON SCHEMA fact TO payload, pophr, read_only;
GRANT USAGE ON SCHEMA obs TO payload, pophr, read_only;
GRANT USAGE ON SCHEMA pophr TO payload, pophr, read_only;
GRANT USAGE ON SCHEMA ref TO payload, pophr, read_only;
GRANT USAGE ON SCHEMA usage TO payload, pophr, read_only;

-- Cubes Permissions
GRANT ALL ON ALL TABLES IN SCHEMA cubes TO payload, read_only, pophr;
GRANT ALL ON ALL SEQUENCES IN SCHEMA cubes TO payload, read_only, pophr;

-- Data Permissions
GRANT ALL ON ALL TABLES IN SCHEMA data TO payload, read_only, pophr;
GRANT ALL ON ALL SEQUENCES IN SCHEMA data TO payload, read_only, pophr;

-- Dim Permissions
GRANT SELECT ON ALL TABLES IN SCHEMA dim TO payload, read_only, pophr;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA dim TO payload, read_only, pophr;

-- Fact Permissions
GRANT ALL ON ALL TABLES IN SCHEMA fact TO payload, read_only, pophr;
GRANT ALL ON ALL SEQUENCES IN SCHEMA fact TO payload, read_only, pophr;

-- Obs Permissions
GRANT SELECT ON ALL TABLES IN SCHEMA obs TO payload, read_only, pophr;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA obs TO payload, read_only, pophr;

-- PopHR Permissions
GRANT SELECT ON ALL TABLES IN SCHEMA pophr TO payload, read_only, pophr;
GRANT SELECT, USAGE ON ALL SEQUENCES  IN SCHEMA pophr TO payload, read_only, pophr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA pophr TO payload, read_only, pophr;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE pophr.causal_concept_to_cubes, pophr.fact, pophr.methodology TO payload;

-- Ref Permissions
GRANT SELECT ON ALL TABLES IN SCHEMA ref TO payload, read_only, pophr;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA ref TO payload, read_only, pophr;

-- Usage Permissions
GRANT INSERT, SELECT, UPDATE, DELETE ON ALL TABLES IN SCHEMA usage TO payload, read_only, pophr;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA usage TO payload, read_only, pophr;

EOSQL
