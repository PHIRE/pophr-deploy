#!/bin/bash
set -e

if [[ ! -v POPHR_PASSWORD ]]; then
  echo "POPHR_PASSWORD environment variable not set"
  exit 1
fi

if [[ ! -v POSTGRES_PASSWORD ]]; then
  echo "POSTGRES_PASSWORD environment variable not set"
  exit 1
fi

psql -v ON_ERROR_STOP=1 \
     --username "$POSTGRES_USER" \
     --dbname pophr \
     -c "ALTER ROLE pophr WITH PASSWORD '$POPHR_PASSWORD';"